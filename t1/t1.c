/**
 *	      @file: t1.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* descricao de metodos */
void mandarMensagem(int vidas, char mensagem[]),
	mandarMensagemComEspera(int vidas, char mensagem[]),
	mandarMensagemComOpcoes(int vidas, char mensagem[], char opcao[3][1000]),
	menu(int vidas),
	mostrarVidas(int vidas);
int lerResposta(), menuDificuldade();
int *fase(int index, const char GAME[][7][1000], int vidas),
	*senha(int vidas);

int main(){
	/* array das telas do jogo */
	const char GAME[][7][1000]={{"Você decidiu aproveitar o seu tempo de folga com sua esposa fazendo um piquenique no parque. Ao chegar no parque, a sua bexiga resolve protestar e você tem que correr para o banheiro.","\0","","",".A",".A",".A"},/* @ */
{"Você está no banheiro quando...\n\nBAAM\n\nA porta se abre de repente e você começa a ouvir uma voz sussurando palavras incompreensíveis de forma estressada.\n\nFLING\n\nVocê escuta um barulho, como uma arma sendo carregada. O que fazer?","Sair e confrontar a voz", "Pegar sua arma e espiar a voz para tentar identificá-la","Esperar a voz sumir","dB",".C",".D"},/* .A */
{"Quando você abriu a porta e chamou a voz, o homem se virou assustado e...\n\nBANG\n\nFoi o último som que você ouviu antes de cair no chão...","\0","","",".A",".A",".A"}, /* .B */
{"Você abre a porta lentamente com sua arma apontada para a voz. Você percebe a pessoa fora de controle conversando consigo mesma:\n\n\t-\"Pegue logo ela e vamos fugir\"\n\t-\"Não posso, é errado sequestrá-la\"\n\t-\"Vamos de uma vez\"\n\t-\"Tá, tá...\"\n\n Rapidamente ela se vira em direção a porta. O que fazer?","Abaixar sua arma e chamar o homem","Dar um tiro na perna dele para impedí-lo","Não fazer nada e deixá-lo sair, não é problema seu no seu dia de folga","dB",".E",".D"}, /* .C */
{"Não demora muito a voz saiu do banheiro. Você lava sua mão e pensa se fez a escolha certa. Antes que possa se preocupar mais com isso, você escuta uma voz feminina gritar de fora do banheiro...","\0","","",".G",".G",".G"}, /* .D */
{"Você mira e acerta a perna direita do homem. Ela joga a arma longe e cai no chão gritando de dor.\n\nBANG\n\nVocê escuta uma mulher gritando à distância. Imediatamente, corre para fora do banheiro...","\0","","",".F",".F",".F"}, /* .E */
{"É tarde demais. Tudo que você pode fazer é observar a sua esposa ser levada para dentro de um SUV preto\n\nNÃAAAOOO\n\nVocê grita em desespero. Imediatamente você percebe um carro acelerando a distância com uma arma na janela. O que você faz?","Volta para o banheiro para interrogar o homem que você atirou","Corre atrás do carro","Atira no carro",".K","dI",".J"}, /* .F */
{"É tarde demais. Tudo que você pode fazer é observar a sua esposa ser levada para dentro de um SUV preto\n\nNÃAAAOOO\n\nVocê grita em desespero. Imediatamente você percebe uma arma na janela do carro. O que você faz?","Anota a placa para fazer uma pesquisa na base de dados do DETRAN","Corre atrás do carro","Atira no carro",".M","dH",".J"}, /* .G */
{"Quando você começa a correr, o passageiro que levava a arma percebe o seu movimento e dispara 3 tiros na sua direção. Você acredita que está tudo bem, mas rapidamente percebe um sangramento intenso no seu ombro. Uma das balas ricocheteou no seu ombro e se alojou no seu coração, te matando rapidamente","\0","","",".G",".G",".G"}, /* .H */
{"Quando você começa a correr, o passageiro que levava a arma percebe o seu movimento e dispara 3 tiros na sua direção. Você acredita que está tudo bem, mas rapidamente percebe um sangramento intenso no seu ombro. Uma das balas ricocheteou no seu ombro e se alojou no seu coração, te matando rapidamente","\0","","",".F",".F",".F"}, /* .I */
{"Você atira em direção ao carro, mas isso não faz difereça, pois o carro é blindado. Você percebe que seu esforço de para o carro será inútil... O que fazer?","Anota a placa para fazer uma pesquisa na base de dados do DETRAN","Roubar um carro para seguir o carro","Esquecer isso e deixar os policiais trabalharem",".M",".O","dP"}, /* .J */
{"Você volta para o banheiro e aponta a arma para a cabeça do homem caído. Depois de alguns minutos utilizando algumas técnicas de interrogatório \"otimizado\" você descobre onde estão os homens que sequestraram sua esposa.","\0","","",".N",".N",".N"}, /* .K */
{"Ao chegar no local, você foi \"recebido\" por 3 homens fortemente armados que não tiverem piedade de você. Você morreu...","\0","","",".N",".N",".N"}, /* .L */
{"Ao rodar uma pesquisa, você descobre o endereço do SUV como sendo em uma área rural da cidade.","\0","","",".N",".N",".N"}, /* .M */
{"Ao analisar o local, você acredita que sua esposa foi mesmo levada para lá. O que fazer?","Ir para o local sem apoio","Ir para o local depois de pegar armas no arsenal da polícia","Buscar apoio da polícia","dL",".Q","dS"}, /* .N */
{"Você segue o carro discretamente até uma fazenda na área rural da cidade. O local parece abandonado, você decide analisar melhor o local antes de fazer algo.","\0","","",".N",".N",".N"}, /* .O */
{"Você sai do caso e espera que a polícia resgate sua esposa. Entretanto, eles não conseguem localizá-la, mesmo depois de 6 meses de busca...\nVocê entrou em depressão profunda e não vê mais sentido em viver...\n\n Você morreu...","\0","","",".J",".J",".J"}, /* .P */
{"Você vai ao arsenal da polícia e se arma até os dentes. Armado, você volta ao local e se prepara para invadí-lo","\0","","",".R",".R",".R"}, /* .Q */
{"Você entra atirando para matar.\nTrês guardas na área exterior e mais três espalhados pela casa principal da fazenda. Você encontra uma sala trancada com uma senha que é um número de 0 a 100, você terá que tentar adivinhá-la","\0","","",".Z",".Z",".Z"}, /* .R */
{"Você sai do caso e espera que a polícia resgate sua esposa. Entretanto, eles não conseguem localizá-la, mesmo depois de 6 meses de busca...\nVocê entrou em depressão profunda e não vê mais sentido em viver...\n\n Você morreu...","\0","","",".N",".N",".N"}, /* .S */};
	int *resultado,
		prox=0,
		vidas=3,
		dificuldade=1;
	do{
		vidas=4-dificuldade;
		prox=0;
		menu(vidas);
		switch(lerResposta()){
			case 3:
				return 0;
			case 1: 
					do{
						resultado = fase(prox,GAME,vidas);
						vidas=*resultado;
						prox=*(resultado+1);				
					}while(vidas && prox!=100);
					if(prox == 100 ){
						do{
							resultado = senha(vidas);
							vidas = *(resultado);
							if(!*(resultado+1)){
								mandarMensagemComEspera(vidas,"Você resgatou sua esposa e vocês conseguiram retornar para casa!");
								break;
							} else {
								mandarMensagemComEspera(vidas, "Você ultrapassou o limite de tentativas e a sala cofre explodiu, matando você e a sua esposa...");
							}
						}while(vidas);
					}
					if(vidas == 0){
						if (prox!=100) resultado = fase(prox, GAME, vidas);
						mandarMensagemComEspera(0,"\n\n\t┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼\n\t███▀▀▀██┼███▀▀▀███┼███▀█▄█▀███┼██▀▀▀\n\t██┼┼┼┼██┼██┼┼┼┼┼██┼██┼┼┼█┼┼┼██┼██┼┼┼\n\t██┼┼┼▄▄▄┼██▄▄▄▄▄██┼██┼┼┼▀┼┼┼██┼██▀▀▀\n\t██┼┼┼┼██┼██┼┼┼┼┼██┼██┼┼┼┼┼┼┼██┼██┼┼┼\n\t███▄▄▄██┼██┼┼┼┼┼██┼██┼┼┼┼┼┼┼██┼██▄▄▄\n\t┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼\n\t███▀▀▀███┼▀███┼┼██▀┼██▀▀▀┼██▀▀▀▀██▄┼\n\t██┼┼┼┼┼██┼┼┼██┼┼██┼┼██┼┼┼┼██┼┼┼┼┼██┼\n\t██┼┼┼┼┼██┼┼┼██┼┼██┼┼██▀▀▀┼██▄▄▄▄▄▀▀┼\n\t██┼┼┼┼┼██┼┼┼██┼┼█▀┼┼██┼┼┼┼██┼┼┼┼┼██┼\n\t███▄▄▄███┼┼┼─▀█▀┼┼─┼██▄▄▄┼██┼┼┼┼┼██▄\n\t┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼\n\n\n");
					}
					
				break;
			case 2:
				dificuldade = menuDificuldade();
				break;
		}
	}while(1);
	return 0;
}

/* Funcao que mostra o menu*/
void menu(int vidas){
	char opcoes[][1000]= {"Iniciar \"descanso\";","Mudar nível de habilidade;","Sair;"};
	mandarMensagemComOpcoes(vidas, "Parabéns, capitão, você mereceu esse dia de folga. Depois de anos combatendo o crime, você merece um dia de descanso. Aproveite-o bem, nunca se sabe quando será o próximo...",opcoes);
}

/* metodo que interpreta a linha da matriz historia */
int *fase(int index, const char GAME[][7][1000], int vidas){
	char mensagem[1000],
		opcoes[3][1000],
		sequencias[3][1000];
	int i;
	static int result[2]; /*{vidasRestantes, proxFase}*/
	strcpy(mensagem,GAME[index][0]);
	for(i=0;i<3;i++){
		strcpy(opcoes[i],GAME[index][i+1]); 
		strcpy(sequencias[i],GAME[index][i+4]);
	}
	int resposta=1;
	if(!strcmp(opcoes[0], ""))
		mandarMensagemComEspera(vidas, mensagem);
	else {
		mandarMensagemComOpcoes(vidas, mensagem, opcoes);
		resposta = lerResposta();
		if(sequencias[resposta-1][0]=='d'){
			vidas--;
		}
	}

	result[0]=vidas;
	result[1]=sequencias[resposta-1][1]=='Z'?100:(sequencias[resposta-1][1]-'@');
	return result;
}

/* função que muda a quantidade de vidas/dificuldade */
int menuDificuldade(){
	char opcoes[][1000]= {"Cabo", "Tenente", "General"};
	mandarMensagemComOpcoes(0, "Quanto você confia nas suas habilidades de policial?",opcoes);
	return lerResposta();
}

/* funcao para mostrar as vidas em forma de coracoes */
void mostrarVidas(int vidas){
	printf("Vidas: ");
	while(vidas--){
		printf("%c%c%c ", '\xE2', '\x99', '\xA5');
	}
	printf("\n");
}

/* funcao para facilitar a exibicao de mensagens na tela */
void mandarMensagem(int vidas, char mensagem[]){
	system("clear");
	mostrarVidas(vidas);
	printf("\n%s\n", mensagem);
}

/* funcao para facilitar a exibicao de mensagens na tela e esperar para continuar */
void mandarMensagemComEspera(int vidas, char mensagem[]){
	mandarMensagem(vidas,mensagem);
	printf("Pressione qualquer tecla para continuar...\n");
	while(getchar()!='\n');	
}

/* funcao para facilitar a exibicao de mensagens com opcao na tela */
void mandarMensagemComOpcoes(int vidas, char mensagem[], char opcao[3][1000]){
	mandarMensagem(vidas, mensagem);
	printf("\n>> 1 - %s\n>> 2 - %s\n>> 3 - %s\n\nResposta: ", opcao[0], opcao[1], opcao[2]);
}

/* abstracao para ler a resposta do usuario */
int lerResposta(){
	int resposta;
	do{
		scanf("%d", &resposta);
		if (resposta>3 || resposta<1){
			printf("\nTá errado!! Quer dizer.. tá certo.. Mas tá errado, tem que ser um número de 1 a 3!\nResposta:");
		}
		while(getchar()!='\n');
	}while(resposta > 3 || resposta < 1);
	return resposta;
}

/* abstracao para ler a resposta do usuario com numeros até 100 */
int lerResposta100(){
	int resposta;
	do{
		scanf("%d", &resposta);
		if (resposta>100 || resposta<0){
			printf("\nEsse número não é válido! Digite um número entre 0 e 100!");
		}
		while(getchar()!='\n');
	}while(resposta > 100 || resposta < 0);
	return resposta;
}

/* funcao do jogo de adivinhar a senha */
int *senha(int vidas){
	int cont=0, tentativa, senha;
	static int retorno[2];
	mandarMensagem(vidas,"ATENÇÃO! Se você errar a senha 7 vezes essa sala cofre se autodestruirá!");
	srand(time(NULL));
	senha=rand()%100;
	for(;cont<7;cont++){
		printf("\nDigite sua tentativa:\n");
		tentativa=lerResposta100();
		if(tentativa == senha){
			retorno[0]=vidas;
			retorno[1]=0;
			return retorno;
		} else if(tentativa>senha){
			printf("É um número menor\n");
		} else if(tentativa<senha){
			printf("É um número maior\n");
		}
	}
	retorno[0]=--vidas;
	retorno[1]=1;
	return retorno;
}
