'''
 	      @file: acimaDiagonalPrincipal.py
 	    @author: Mateus Berardo de Souza Terra 17/0018806
 	@disciplina: Algoritmos e Programacao de Computadores
 
 
 '''


if __name__ == "__main__":
	mat = [[] for i in range(12)]
	op = input('')
	for line in mat:
		for i in range(12):
			line.append(float(input('')))
		
	n=0
	soma=0
	for index, line in enumerate(mat):
		for column in line[:index]:
			soma+=column
			n+=1

	print("{:.1f}".format(soma if op=='S' else soma/n))