#include <stdio.h>

unsigned long factorial(int x){
	if(x==1) return 1;
	else return x*factorial(x-1);
}

int main(){
	printf("%lu",factorial(19));
	return 0;
}


