public class FactorialRecursive{
	public static void main(String args[]){
		System.out.println(factorial(15));
	}
	public static long factorial(int x){
		if(x==1) return 1;
		else return x*factorial(x-1); 
	}
}
