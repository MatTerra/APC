/**
 *	      @file: seqSum.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n1, n2;
	scanf("%d %d", &n1, &n2);
	do{
		if(n2<n1){
			printf("Decrescente\n");
		} else {
			printf("Crescente\n");
		}
		scanf("%d %d", &n1, &n2);
	}while(n1!=n2);
	return 0;
}
