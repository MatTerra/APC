/**
 *	      @file: validacaoDeNota.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

float lerNota(){
	float i;
	scanf("%f",&i);
	while(i>10.0 || i<0){
		printf("nota invalida\n");
		scanf("%f",&i);
	}
	return i;
}
int main(){
	float n1, n2;
	n1=lerNota();
	n2=lerNota();
	printf("media = %.2f\n", (n1+n2)/2);
	return 0;
}
