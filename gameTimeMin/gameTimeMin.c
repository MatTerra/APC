/*	      @file: gameTimeMin
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int start, end, startMin, endMin, hours, minutes;
	scanf("%d %d %d %d", &start, &startMin, &end, &endMin);
	if(end > start){
		hours = end-start;
	} else if( end < start ){
		hours = (24-start)+end;
	} else {
		hours = 24;
	}
	if(endMin > startMin){
		minutes = endMin - startMin;
	} else if( endMin < startMin ){
		minutes = (60-startMin)+endMin;
		hours--;
	}
	printf("O JOGO DUROU %d HORA(S) E %d MINUTO(S)\n", hours, minutes);
	return 0;
}
