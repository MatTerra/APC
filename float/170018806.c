/**      @file: IEEE754.c
 *     @author: Mateus Berardo de Souza Terra 17/0018806
 * @disciplina: Algoritmos e Programação de Computadores
 *
 * Implemente a função IEEE754_32. Considere que o usuário
 * sabe utilizar corretamente a função (ou seja, todas as
 * entradas serão válidas). Você pode testar os valores em:
 * http://www.h-schmidt.net/FloatConverter/IEEE754.html. */

#include <stdio.h>
#include <math.h>

/* Lê um conjunto de 32 bits e retorna o valor númerico (base 10) que estes bits
representam considerando o padrão IEEE 754. */
float IEEE754_32() {
	float value=0;
	char bitSig, bitsExp[8],bitsMantissa[24];
	int signal=1, exp=0,i;
	scanf("%c%8s%23s", &bitSig, bitsExp, (bitsMantissa+1));
	if(bitSig=='1'){
		signal=-1;
	}
	for(i=7;i>=0;i--){
		if(bitsExp[i]=='1') 
			exp+=pow(2,7-i);
	}
	exp-=127;
	bitsMantissa[0]='1';
	for(i=0; i<24;i++){
		if(bitsMantissa[i]=='1')
			value+=pow(2,exp-i);
	}
	return value*signal;
}

int main() {
    printf("Digite 32 bits: ");
    printf("%f\n", IEEE754_32());
    return 0;
}
