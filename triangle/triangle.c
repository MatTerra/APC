#include <stdio.h>

float sides[3];

void bubbleSort(){
	int t = 1;
	int swap;
	while(t==1){
		t=0;
		int x=0;
		while(x<2){
			if(sides[x] < sides[x+1]){
				t = 1;
				swap=sides[x];
				sides[x]=sides[x+1];
				sides[x+1]=swap;
			}
			x++;
		}
	}
}


int main(){
	scanf("%f %f %f", &sides[0],&sides[1],&sides[2]);
	bubbleSort();
	if(sides[0]>=sides[1]+sides[2]){
		printf("Area = %.1f\n", ((sides[0]+sides[1])/2)*sides[2]);
	}else{
		printf("Perimetro = %.1f\n", sides[0]+sides[1]+sides[2]);
	}	
	return 0;
}

