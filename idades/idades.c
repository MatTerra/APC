/**
 *	      @file: idades.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int sum=0, n=0, idade;
	scanf("%d", &idade);
	do{
		sum+=idade;
		n++;
		scanf("%d",&idade);
	}while(idade>0);
	printf("%.2f\n", (float)sum/n);
	return 0;
}
