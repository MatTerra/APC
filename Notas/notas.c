/*	@file 		notas.c
 *	@author		Mateus Berardo de Souza Terra
 *	@disciplina	APC - Algoritmos e Programação de Computadores
 *
 *	Calcula a menor quantidade de notas e moedas para inteirar um valor
 *	fornecido pelo usuário
 */

#include <stdio.h>

int nota100, nota50, nota20, nota10, nota5, nota2, nota1, valor;

int main(){
	scanf("%d", &valor);
	nota100 = valor/100;
	valor = valor-(nota100*100);
	nota50 = valor/50;
	valor = valor-(nota50*50);
	nota20 = valor/20;
	valor = valor-(nota20*20);
	nota10 = valor/10;
	valor = valor-(nota10*10);
	nota5 = valor/5;
	valor = valor-(nota5*5);
	nota2 = valor/2;
	valor = valor-(nota2*2);
	nota1 = valor;
	printf("%d nota(s) de R$ 100,00\n%d nota(s) de R$ 50,00\n%d nota(s) de R$ 20,00\n", nota100, nota50, nota20);
	printf("%d nota(s) de R$ 10,00\n%d nota(s) de R$ 5,00\n%d nota(s) de R$ 2,00\n", nota10, nota5, nota2);
	printf("%d nota(s) de R$ 1,00\n", nota1);
	return 0;
}
