'''	      @file: fibonnaciFacil.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
def fibonacci(n):
	fib=0
	if n==0:
		fib=0
	elif n<2:
		fib=1
	else:
		fib=fibonacci(n-1)+fibonacci(n-2)
	return fib
n=int(input())
for i in range(0,n):
	END=' '
	if (i==n-1):
		END = '\n'
	print(fibonacci(i), end=END)
