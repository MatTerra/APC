/**
 *	      @file: fibonnaciFacil.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

double fib[500];


double fibonacci(int n){
	if(n==0){
		fib[n]=0;
		return fib[n];
	}else if(n<3){
		fib[n]=1;
		return fib[n];
	}
	fib[n] = (fib[n-1]!=0?fib[n-1]:fibonacci(n-1))+(fib[n-2]!=0?fib[n-2]:fibonacci(n-2));
	return fib[n];
}

int main(){
	int n, i, cont=0;
	char end=' ';
	scanf("%d", &n);
	for(i=0; i<=cont; i++)
        	fib[i] = 0; 
	while(cont<n){
		if(cont==n-1) end = '\n';
		printf("%.0lf%c", fibonacci(cont),end);
		cont++;
	}
	return 0;
}
