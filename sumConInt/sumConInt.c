/**
 *	      @file: sumConInt.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int x, n, sum=0;
	scanf("%d %d", &x, &n);
	while(n<=0)
		scanf("%d", &n);
	while(n--){
		sum += x;
		x++;
	}
	printf("%d\n",sum);
	return 0;
}
