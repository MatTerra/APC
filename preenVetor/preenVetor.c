/**
 *	      @file: preenVetor.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int i=1, N[10];
	scanf("%d", &N[0]);
	printf("N[0] = %d\n", N[0]);
	for(;i<10;i++){
		N[i]=2*N[i-1];
		printf("N[%d] = %d\n", i, N[i]);
	}
	return 0;
}
