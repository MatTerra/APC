/**
 *	      @file: coordinate.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int x,y;
	scanf("%d %d", &x, &y);
	do{
		if(x>0.0){
			if(y>0){
				printf("primeiro\n");
			} else if (y<0){
				printf("quarto\n");
			}
		} else if(x<0.0) {
			if(y>0){
				printf("segundo\n");
			} else if (y<0){
				printf("terceiro\n");
			}
		}
		scanf("%d %d", &x, &y);
	} while (x!=0 && y !=0);
	return 0;
}

