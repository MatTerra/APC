/**
 *	      @file: seqSum.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n1, n2, aux,soma;
	scanf("%d %d", &n1, &n2);
	do{
		soma=0;
		if(n2<n1){
			aux = n2;
			n2=n1;
			n1=aux;
		}
		for(;n1<=n2;n1++){
			printf("%d ", n1);
			soma+=n1;
		}
		printf("Sum=%d\n", soma);
		scanf("%d %d", &n1, &n2);
	}while(n1>0 && n2>0);
	return 0;
}
