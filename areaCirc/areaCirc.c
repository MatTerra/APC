/**	      @file: areaCirc.c
 *    	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *	Calcula a area de um de um circulo com base na entrada e mostra na saida padrao
 */

#include <stdio.h>

const double pi = 3.14159;
double R;

int main(){
	scanf("%lf", &R);
	printf("A=%.4lf\n",R*R*pi);
	return 0;
}
