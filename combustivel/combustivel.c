/**
 *	      @file: combustivel.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int a=0, g=0, d=0, n;
	scanf("%d", &n);
	do{
		switch(n){
			case 1:
				a++;
				break;
			case 2:
				g++;
				break;
			case 3:
				d++;
				break;
		}
		scanf("%d", &n);
	}while(n!=4);
	printf("MUITO OBRIGADO\nAlcool: %d\nGasolina: %d\nDiesel: %d\n",a,g,d);
	return 0;
}
