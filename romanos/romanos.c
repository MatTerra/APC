/**
 *	      @file: romanos.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int pg, centena, dezena, unidade;
	scanf("%d", &pg);
	centena = pg/100;
	dezena = (pg/10)-(centena*10);
	unidade = pg-(centena*100)-(dezena*10);
	if(centena<4){
		while(centena>0){
			printf("C");
			centena--;
		}
	}else if(centena == 4){
		printf("CD");
	}else if(centena < 9){
		printf("D");
		centena=centena-5;
		while(centena>0){
			printf("C");
			centena--;
		}
	}else if(centena == 9){
		printf("CM");
	}else{
		printf("M");
	}

	if(dezena<4){
		while(dezena>0){
			printf("X");
			dezena--;
		}
	}else if(dezena == 4){
		printf("XL");
	}else if(dezena < 9){
		printf("L");
		dezena=dezena-5;
		while(dezena>0){
			printf("X");
			dezena--;
		}
	}else if(dezena == 9){
		printf("XC");
	}else{
		printf("C");
	}

	if(unidade<4){
		while(unidade>0){
			printf("I");
			unidade--;
		}
	}else if(unidade == 4){
		printf("IV");
	}else if(unidade < 9){
		printf("V");
		unidade=unidade-5;
		while(unidade>0){
			printf("I");
			unidade--;
		}
	}else if(unidade == 9){
		printf("IX");
	}else{
		printf("X");
	}
	printf("\n");
	return 0;
}
