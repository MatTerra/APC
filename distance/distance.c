/*	      @file: distance
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	const float v = 0.5;
	int d;
	scanf("%d",&d);
	printf("%.0f minutos\n",d/v);
	return 0;
}
