/*		@file: soma.c
 *	      @author: Mateus Berardo de Souza Terra 17/0018806
 *	  @disciplina: Algoritmos e Programacao de Computadores
 *
 * 	Recebe dois inteiros A e B e mostra a soma X deles na saida padrao
 *
 */

#include <stdio.h>

double A, B, C, media;

int main(){
	scanf("%lf\n%lf\n%lf", &A, &B, &C);
	media = ((A*2)+(B*3)+(C*5))/10;
	printf("MEDIA = %.1lf\n", media);
	return 0;
}
