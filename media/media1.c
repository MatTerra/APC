/*		@file: soma.c
 *	      @author: Mateus Berardo de Souza Terra 17/0018806
 *	  @disciplina: Algoritmos e Programacao de Computadores
 *
 * 	Recebe dois inteiros A e B e mostra a soma X deles na saida padrao
 *
 */

#include <stdio.h>

double A, B, media;

int main(){
	scanf("%lf\n%lf", &A, &B);
	media = ((A*3.5)+(B*7.5))/11;
	printf("MEDIA = %.5lf\n", media);
	return 0;
}
