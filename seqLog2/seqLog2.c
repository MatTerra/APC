/**
 *	      @file: seqLog2.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int col, n, colCont, cont=1;
	scanf("%d %d", &col, &n);
	while(cont<=n){
		colCont = col;
		while(colCont--){
			if(colCont+1<col) printf(" ");
			printf("%d", cont);
			cont++;
		}
		printf("\n");
	}
	return 0;
}
