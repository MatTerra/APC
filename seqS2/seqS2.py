'''	      @file: seqS2.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
sum=0
exp=0
for i in range(1,40,2):
	sum+=i/(2**exp)
	exp+=1
print("%.2f"%sum)
