/**
 *	      @file: t3.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *  Implementacao de um CRUD que simula operações bancarias */

#define MAX_NAME_LENGTH 101
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[38;2;00;95;255m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_ORANGE  "\x1b[38;2;255;95;00m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

typedef struct{
    int dia, mes, ano;
} __attribute__ ((packed)) data_t;

typedef struct{
    char nome[MAX_NAME_LENGTH];
    data_t aniversario;
    char senha[13];
    float saldo;    
} __attribute__ ((packed)) cliente_t;

int gravarCliente(cliente_t *cliente),
    lerCliente(cliente_t *cliente), 
    criarCliente(),
    abrirCliente(char *nome, char *modo, FILE *arquivoCliente), 
    getChave(char *chave),  
    encriptar(char *senha), 
    lerSenha(char *senha),
    consultaConta(),
    menu(),
    lerResposta();
const char *getPath(char *nome);
void replace(char *caractere, char um, char outro), 
    lerNome(cliente_t *cliente), 
    lerAniversario(data_t *data);

int main(){
    int sair=0;
    while(!sair) sair = menu();
    return 0;
}

int menu(){
    system("clear");
    int opcao;
    printf(ANSI_COLOR_BLUE "    FFFFFFFFFFFFFFFFFFFFFF               AAA               IIIIIIIIIIXXXXXXX       XXXXXXX               AAA               \n");
    printf(ANSI_COLOR_BLUE "    F::::::::::::::::::::F              A:::A              I::::::::IX:::::X       X:::::X              A:::A              \n");
    printf(ANSI_COLOR_BLUE "    F::::::::::::::::::::F             A:::::A             I::::::::IX:::::X       X:::::X             A:::::A             \n");
    printf(ANSI_COLOR_BLUE "    FF::::::FFFFFFFFF::::F            A:::::::A            II::::::IIX::::::X     X::::::X            A:::::::A            \n");
    printf(ANSI_COLOR_BLUE "      F:::::F       FFFFFF           A:::::::::A             I::::I  XXX:::::X   X:::::XXX           A:::::::::A           \n");
    printf(ANSI_COLOR_BLUE "      F:::::F                       A:::::A:::::A            I::::I     X:::::X X:::::X             A:::::A:::::A          \n");
    printf(ANSI_COLOR_BLUE "      F::::::FFFFFFFFFF            A:::::A A:::::A           I::::I      X:::::X:::::X             A:::::A A:::::A         \n");
    printf(ANSI_COLOR_BLUE "      F:::::::::::::::F           A:::::A   A:::::A          I::::I       X:::::::::X             A:::::A   A:::::A        \n");
    printf(ANSI_COLOR_BLUE "      F:::::::::::::::F          A:::::A     A:::::A         I::::I       X:::::::::X            A:::::A     A:::::A       \n");
    printf(ANSI_COLOR_BLUE "      F::::::FFFFFFFFFF         A:::::"ANSI_COLOR_ORANGE"AAAAAAAAA"ANSI_COLOR_BLUE":::::A        I::::I      X:::::X:::::X          A:::::AAAAAAAAA:::::A      \n");
    printf(ANSI_COLOR_BLUE "      F:::::F                  A:::::"ANSI_COLOR_ORANGE":::::::::::"ANSI_COLOR_BLUE":::::A       I::::I     X:::::X X:::::X        A:::::::::::::::::::::A     \n");
    printf(ANSI_COLOR_BLUE "      F:::::F                 A:::::"ANSI_COLOR_ORANGE"AAAAAAAAAAAAA"ANSI_COLOR_BLUE":::::A      I::::I  XXX:::::X   X:::::XXX    A:::::AAAAAAAAAAAAA:::::A    \n");
    printf(ANSI_COLOR_BLUE "    FF:::::::FF              A:::::A             A:::::A   II::::::IIX::::::X     X::::::X   A:::::A             A:::::A   \n");
    printf(ANSI_COLOR_BLUE "    F::::::::FF             A:::::A               A:::::A  I::::::::IX:::::X       X:::::X  A:::::A               A:::::A  \n");
    printf(ANSI_COLOR_BLUE "    F::::::::FF            A:::::A                 A:::::A I::::::::IX:::::X       X:::::X A:::::A                 A:::::A \n");
    printf(ANSI_COLOR_BLUE "    FFFFFFFFFFF           AAAAAAA                   AAAAAAAIIIIIIIIIIXXXXXXX       XXXXXXXAAAAAAA                   AAAAAAA\n\n"ANSI_COLOR_RESET);
    printf(ANSI_COLOR_ORANGE"\t\t\t╔═══════════════════════╦═══════════════════════╦═══════════════════════╗\n");
    printf("\t\t\t║  I  - Criar conta     ║   II  - Alterar dados ║ III - Consultar conta ║\n");
    printf("\t\t\t╠═══════════════════════║═══════════════════════║═══════════════════════╣\n");
    printf("\t\t\t║  IV - Consultar conta ║    V  - Transferência ║  VI - Depósito        ║\n");
    printf("\t\t\t╠═══════════════════════║═══════════════════════║═══════════════════════╣\n");
    printf("\t\t\t║ VII - Saque           ║  VIII - Sair          ║                       ║\n");
    printf("\t\t\t╚═══════════════════════╩═══════════════════════╩═══════════════════════╝\n\n\t"ANSI_COLOR_BLUE">> "ANSI_COLOR_RESET);
    opcao = lerResposta();
    switch(opcao){
        case 1:
            if(criarCliente()==EXIT_SUCCESS){
                system("clear");
                printf(ANSI_COLOR_GREEN"Seja bem-vindo à Faixa, obrigado pela preferência!\n");
                getchar();
            }else{
                printf(ANSI_COLOR_RED"Não foi possível criar uma conta no momento, tente novamente mais tarde"ANSI_COLOR_RESET);
                getchar();
            }
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            if(consultaConta()==EXIT_FAILURE)
                printf(ANSI_COLOR_RED"Não foi possível consultar essa conta no momento. Você digitou corretamente o nome e possui uma conta?\n"ANSI_COLOR_RESET);
                getchar();
            break;
        case 5:
            break;
        case 6:
            break;
        case 7:
            break;
        default:
            return 1;
            break;
    }
    return 0;
}

/* transforma o nome em um caminho de arquivo binário */        
const char *getPath(char *nome){
    static char path[MAX_NAME_LENGTH+4];
    replace(nome, ' ', '-');
    sprintf(path, "%s.bin", nome);
    return path;
} 

/* substitui o caractere 'um' pelo 'outro' na string fornecida */
void replace(char *caractere, char um, char outro){
    if(*caractere=='\0')
        return;
    if(*caractere==um)
        *caractere=outro;
    return replace(caractere+1, um, outro);
}  

/* le e valida o nome */
void lerNome(cliente_t *cliente){
    int ok;
    do{
        ok=1;
        printf(ANSI_COLOR_BLUE"Por favor, insira seu nome"ANSI_COLOR_YELLOW"(máximo 100 caracteres)"ANSI_COLOR_BLUE": \n\t>> ");
        scanf("%100[^\n]", cliente->nome);
        if(getchar()!='\n'){
            printf(ANSI_COLOR_RED"Esse nome é muito comprido, por favor abrevie algum sobrenome!\n"ANSI_COLOR_RESET);
            while(getchar()!='\n');
            ok = 0;
        }
    }while(!ok);
} 

/* le e valida a data de nascimento */
void lerAniversario(data_t *data){
    int ok;
    do{
        ok=1;
        printf(ANSI_COLOR_BLUE"Por favor, insira sua data de nascimento"ANSI_COLOR_YELLOW"(dd/mm/aaaa)"ANSI_COLOR_BLUE": \n\t>> ");
        scanf("%d/%d/%d", &data->dia, &data->mes, &data->ano);
        if((data->dia)>31 || (data->dia)<=0){
            printf(ANSI_COLOR_RED"Esse dia não existe, por favor corrija a data!\n"ANSI_COLOR_RESET);
            ok=0;
        }
        if(data->mes==2 && (data->dia)>29){
            printf(ANSI_COLOR_RED"Esse dia não existe, por favor corrija a data!\n"ANSI_COLOR_RESET);
            ok=0;
        } else if((data->mes)>12||(data->mes)<=0){
            printf(ANSI_COLOR_RED"Esse mês não existe, por favor corrija a data!\n"ANSI_COLOR_RESET);
            ok=0;
        }
        if ((data->ano)>2001){
            printf(ANSI_COLOR_RED"Você deve ter ao menos 16 anos para abrir uma conta na Faixa!\n"ANSI_COLOR_RESET);
            ok=0;
        } else if((data->ano)<1880){
            printf(ANSI_COLOR_RED"Você deve estar vivo para ter uma conta na Faixa!\n"ANSI_COLOR_RESET);
            ok=0;
        }
        if(getchar()!='\n'){
            printf(ANSI_COLOR_RED"Insira apenas sua data de nascimento!\n"ANSI_COLOR_RESET);
            while(getchar()!='\n');
            ok=0;
        }
    }while(!ok);
}

/* le e valida a resposta do usuario */
int lerResposta(){
	int resposta;
	do{
		scanf("%d", &resposta);
		if (resposta > 8 || resposta < 1){
			printf(ANSI_COLOR_BLUE"\nPor favor, escolha uma opção válida!\n\t>> ");
		}
		while(getchar()!='\n');
	}while(resposta > 8 || resposta < 1);
	return resposta;
}

/* cria um novo cliente no banco 
   retorna 0 se criou e 1 se não criou*/
int criarCliente(){
    cliente_t novoCliente;
    printf(ANSI_COLOR_BLUE"Seja bem-vindo à Faixa! Agradecemos a preferência!\n\n"ANSI_COLOR_RESET);
    lerNome(&novoCliente);
    while(lerCliente(&novoCliente) != EXIT_FAILURE){        
        printf(ANSI_COLOR_RED"Já existe um cliente cadastrado com esse nome!!"ANSI_COLOR_RESET);
        lerNome(&novoCliente);
    }
    lerAniversario(&novoCliente.aniversario);
    if(lerSenha(novoCliente.senha)==EXIT_FAILURE)
        return EXIT_FAILURE;
    novoCliente.saldo=0.00;
    return gravarCliente(&novoCliente);
} 
    
/* salva os dados do cliente no arquivo binário correspondente 
   retorna 0 se leu e 1 se não leu */    
int gravarCliente(cliente_t *cliente){
    const char *path = getPath(cliente->nome);
    FILE *cliente_f=fopen(path, "wb+");
    if(cliente_f){
        fwrite(cliente, sizeof(cliente_t), 1, cliente_f);
        fclose(cliente_f);
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

/* recupera o registro do cliente a partir do arquivo. 
   retorna 0 se gravou e 1 se não gravou */
int lerCliente(cliente_t *cliente){
    const char *path = getPath(cliente->nome);
    FILE *cliente_f=fopen(path,"rb");
    if(cliente_f){
        fread(cliente, sizeof(cliente_t), 1, cliente_f);
        fclose(cliente_f);
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

int consultaConta(){
    system("clear");
    cliente_t cliente;
    printf(ANSI_COLOR_ORANGE"\t\tIII - "ANSI_COLOR_BLUE"CONSULTA CONTA\n\n"ANSI_COLOR_RESET);
    lerNome(&cliente);
    if(lerCliente(&cliente)==EXIT_FAILURE)
        return EXIT_FAILURE;
    printf(ANSI_COLOR_BLUE"Nome: "ANSI_COLOR_CYAN"%s\n", cliente.nome);
    printf(ANSI_COLOR_BLUE"Nascimento: "ANSI_COLOR_CYAN"%d/%d/%d\n", cliente.aniversario.dia, cliente.aniversario.mes, cliente.aniversario.ano);
    printf(ANSI_COLOR_BLUE"Saldo: "ANSI_COLOR_CYAN"%.2f\n"ANSI_COLOR_RESET, cliente.saldo);
    getchar();
    return EXIT_SUCCESS;
}

/* le, valida e encripta a senha */
int lerSenha(char *senha){
    int i;
    printf(ANSI_COLOR_BLUE"Insira sua senha de 12 caracteres"ANSI_COLOR_YELLOW" (somente minúsculas) "ANSI_COLOR_BLUE":\n\t>> ");
    scanf("%13[a-zA-Z]", senha);
    while(strlen(senha)!=12 || getchar()!='\n' ){
        printf(ANSI_COLOR_RED"Sua senha deve conter 12 caracteres!\n"ANSI_COLOR_RESET);
        if(!feof(stdin)){
            while(getchar()!='\n');
        }
        printf(ANSI_COLOR_BLUE"Insira sua senha de 12 caracteres"ANSI_COLOR_YELLOW" (somente minúsculas) "ANSI_COLOR_BLUE":\n\t>> ");
        scanf("%13[a-zA-Z]", senha);
    }
    for(i = 0; i<12; i++){
        senha[i] = tolower(senha[i]);
    } 
    if(encriptar(senha)==EXIT_FAILURE)
        return EXIT_FAILURE;
    return EXIT_SUCCESS;
}

/* criptografa a senha */ 
int encriptar(char *senha){
    char chave['z'-'a'];
    int i;
    if(getChave(chave) != EXIT_FAILURE){
        for(i = 0; i<12; i++){
            senha[i] = chave[senha[i]-'a'];
        }
        return EXIT_SUCCESS;
    } 
    return EXIT_FAILURE;
}

/* le o arquivo que contem a chave de criptografia */
int getChave(char *chave){
    FILE *chave_f = fopen("chave.txt", "r");
    if(chave_f){
        fread(chave, sizeof(char), ('z'-'a'), chave_f);
        fclose(chave_f);
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}
