/*	      @file: distanceOfPoints
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>
#include <math.h>

int main(){
	double x1, y1, x2, y2, dx, dy, sqd;
	scanf("%lf %lf\n%lf %lf", &x1, &y1, &x2, &y2);
	dx = x2-x1;
	dy = y2-y1;
	sqd = (dx*dx) + (dy*dy);
	double d = sqrt(sqd);
	printf("%.4lf\n", d);
	return 0;
}
