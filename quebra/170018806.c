/**      @file: quebra.c
 *     @author: Guilherme N. Ramos (gnramos@unb.br)
 * @disciplina: Algoritmos e Programação de Computadores
 *
 * Esta é a questão 2 da Olimpíada Brasileira de Informática – OBI2015
 *
 * Jade precisa da sua ajuda para montar o quebra-cabeças que ela ganhou de
 * presente da sua tia Zoraide! As peças são encaixadas lado a lado e contêm,
 * cada uma, uma letra maiúscula. Quando o quebra-cabeças estiver montado, a
 * sequência de letras revelará uma frase secreta. Cada peça possui, além da
 * letra, dois números: um na parte esquerda e outro na parte direita. Uma peça
 * se encaixa depois de outra, na sequência, quando seu número esquerdo for
 * igual ao número direito da outra peça. O número esquerdo da primeira peça é
 * sempre o 0 (zero) e o número direito da última peça é sempre o 1 (um). Cada
 * número aparece no máximo uma vez na parte esquerda de alguma peça, e no
 * máximo uma vez na parte direita. Sempre é possível encaixar todas as peças e
 * em apenas uma única sequência!
 *
 * Entrada
 *
 * A primeira linha da entrada contém um número natural N, indicando o número
 * de peças do quebra-cabeças. As N linhas seguintes contêm, cada uma, a
 * descrição de uma peça na forma E C D, onde: E é o número esquerdo; C é a
 * letra maiúscula; e D é o número direito.
 *
 * Saída
 *
 * Seu programa deve escrever uma única linha na saída, contendo a sequência de
 * letras formada quando o quebra-cabeças está montado.
 *
 * Restrições:
 *
 *   3 ≤= N ≤= 100000; 0 ≤= E ≤= 200000; e 0 ≤= D ≤= 200000
 *
 *   Há exatamente uma maneira de montar o quebra-cabeças utilizando todas as
 *   peças dadas. */

#include <stdio.h>

typedef struct {
    int direito, esquerdo;
    char letra;
} peca_t;

int main() {
    peca_t pecas[100000];
    peca_t *peca;
    char palavra[100000];
    int N, i, proximo, index;
    scanf("%d", &N);
    for(i=0; i<N; i++){
        scanf("%d %c %d", &((pecas+i)->esquerdo), &((pecas+i)->letra), &((pecas+i)->direito));
        if((pecas+i)->esquerdo==0)
            peca=pecas+i;
    }
    palavra[0]=peca->letra;
    index=1;
    while(peca->direito!=1){
        i=0;
        while((pecas+i)->esquerdo!=peca->direito)
            i++;
        peca=pecas+i;
        palavra[index]=peca->letra;
        index++;
    }
    palavra[index]='\0';
    printf("%s\n", palavra);
    return 0;
}
