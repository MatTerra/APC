/**
 *	      @file: vetor.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int X[10], i=0;
	for(;i<10;i++){
		scanf("%d\n", &X[i]);
		printf("X[%d] = %d\n", i, X[i]>0?X[i]:1);
	}
	return 0;
}
