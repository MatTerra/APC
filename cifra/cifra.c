/**      @file: cifra_de_apc.c
 *     @author: Guilherme N. Ramos (gnramos@unb.br)
 * @disciplina: Algoritmos e Programação de Computadores
 *
 * Implemente as funções cifra() e decifra(). As funções recebem um valor
 * inteiro positivo X (0 <= X <= 26) como argumento, e então lêem uma quantidade
 * indeterminada de caracteres (ASCII) para cifrá-los ou decifrá-los,
 * respectivamente. A entrada de caracteres termina com um espaço ou quebra de
 * linha.
 *
 * A cifra é simples, basta trocar cada letra pelo equivalente em X posições à
 * direita no alfabeto. Ela mantém a caixa da letra (maiúsculas são cifradas
 * em maiúsculas, e minúsculas em minúsculas). A cifra também é "circular", ou
 * seja, se as X posições excederem a última letra, continue a partir da
 * primeira. Por exemplo, supondo X = 3:
 *
 * Entrada: abcdefghijklmnopqrstuvwxyz
 *   Saída: defghijklmnopqrstuvwxyzabc
 *
 * E para X = 17:
 *
 * Entrada: abcdefghijklmnopqrstuvwxyz
 *   Saída: rstuvwxyzabcdefghijklmnopq
 *
 * Obviamente, para recuperar a mensagem original é preciso trocar a letra por X
 * posições à esquerda no alfabeto.
 *
 * Exemplos:
 *
 *                    | Entrada | Saída
 *     ---------------+---------+------
 *     cifra(2)       | ABCde   | CDEfg
 *     cifra(3)       | abcDE   | defGH
 *     cifra(26)      | abc     | abc
 *     cifra(0)       | abc     | abc
 *     cifra(5)       | Cifra   | Hnkwf
 *     cifra(11)      | de      | op
 *     cifra(17)      | Cesar   | Tvjri
 *     decifra(2)     | CDEfg   | ABCde
 *     decifra(3)     | defGH   | abcDE
 *     decifra(26)    | abc     | abc
 *     decifra(0)     | abc     | abc
 *     decifra(5)     | Hnkwf   | Cifra
 *     decifra(11)    | op      | de
 *     decifra(17)    | Tvjri   | Cesar    */

#include <stdio.h>

void cifra(int X) {
	char c, cifrado;
	scanf("%c",&c);
	do{
		cifrado=c+X;
		if(c>'Z'){
			printf("%c", cifrado>'z'||cifrado<0?(c-('z'-'a'+1))+X:cifrado);
		} else {
			printf("%c", cifrado>'Z'||cifrado<0?(c-('Z'-'A'+1))+X:cifrado);
		}
	}while(scanf("%1[^ \n]", &c));
}

void decifra(int X) {
	cifra(('Z'-'A')-X);
}

int main() {
	decifra(17);
    	return 0;
}
