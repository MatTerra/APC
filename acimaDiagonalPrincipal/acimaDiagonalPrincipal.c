/**
 *	      @file: acimaDiagonalPrincipal.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float matriz[12][12], soma;
	int i,j,n=0;
	char op;
	scanf("%c",&op);
	for(i=0; i<12; i++){
		for(j=0; j<12; j++){
			scanf("%f",&matriz[i][j]);
		}
	}
	for(i=0; i<12; i++){
		for(j=11; j>i; j--){
			soma+=matriz[i][j];
			n++;
		}
	}
	printf("%.1f\n", op=='S'?soma:soma/n);

	return 0;
}
