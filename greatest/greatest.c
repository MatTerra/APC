/*	      @file: greatest
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int A, B, C, M, Mod;
	scanf("%d %d %d",&A,&B,&C);
	Mod = A-B;
	if (Mod < 0){
		Mod = -Mod;
	}
	M=(A+B+Mod)/2;
	Mod = M-C;
        if (Mod < 0){
                Mod = -Mod;
        }
        M=(M+C+Mod)/2;
	printf("%d eh o maior\n", M);
	return 0;
}
