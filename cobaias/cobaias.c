/**
 *	      @file: cobaias.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	char animal;
	float pCoelhos, pRatos, pSapos, exp, total=0, tCoelhos=0, tRatos=0, tSapos=0, quant;
	scanf("%f", &exp);
	while(exp>0){
		scanf("%f %c", &quant, &animal);
		switch(animal){
			case 'C':
				tCoelhos+=quant;
				break;
			case 'R':
				tRatos+=quant;
				break;
			case 'S':
				tSapos+=quant;
				break;
		}
		total+=quant;
		exp--;
	}
	pCoelhos=(tCoelhos/total)*100;
	pRatos=(tRatos/total)*100;
	pSapos=(tSapos/total)*100;
	printf("Total: %.0f cobaias\nTotal de coelhos: %.0f\nTotal de ratos: %.0f\nTotal de sapos: %.0f\nPercentual de coelhos: %.2f %%\nPercentual de ratos: %.2f %%\nPercentual de sapos: %.2f %%\n", total, tCoelhos, tRatos, tSapos, pCoelhos, pRatos, pSapos);
	return 0;
}
