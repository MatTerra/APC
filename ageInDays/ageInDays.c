/**
 *	      @file: ageInDays.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int days, months, years;
	scanf("%d", &days);
	years = days/365;
	days = days - (years*365);
	months = days/30;
	days = days - (months*30);
	printf("%d ano(s)\n%d mes(es)\n%d dia(s)\n", years, months, days); 
	return 0;
}
