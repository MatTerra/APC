/**
 *	      @file: linhaNaMatriz.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float matriz[12][12], soma;
	int l,i,c;
	char op;
	scanf("%d\n%c",&l,&op);
	for(i=0; i<12; i++){
		for(c=0; c<12; c++){
			scanf("%f",&matriz[i][c]);
		}
	}
	for(i=0; i<12; i++){
		soma+=matriz[i][l];
	}
	printf("%.1f\n", op=='S'?soma:soma/12);
	return 0;
}
