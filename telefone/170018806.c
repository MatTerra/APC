/**		  @file: 17/0018806
 *		@author: Mateus Berardo de Souza Terra 17/0018806
 *	    @disciplina: Algoritmos e Programacao de Computadores
 *
 * Mostra duas frases em linhas distintas na saida padrao
 */

#include <stdio.h>

int main(){
	printf("%s\n","Telefone");
	printf("%s", "minha Casa...");
	return 0;
}

