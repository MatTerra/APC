'''	      @file: divisores.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
casos=int(input())
for c in range(casos):
	n=int(input())
	sum=0
	for i in range(1,n):
		if(n%i==0):
			sum+=i;
	if sum == n:
		print("%d eh perfeito"%n)
	else:
		print("%d nao eh perfeito"%n)

