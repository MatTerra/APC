/*		@file: soma.c
 *	      @author: Mateus Berardo de Souza Terra 17/0018806
 *	  @disciplina: Algoritmos e Programacao de Computadores
 *
 * 	Recebe dois inteiros A e B e mostra a soma X deles na saida padrao
 *
 */

#include <stdio.h>

int A, B, X;

int main(){
	scanf("%d\n%d", &A, &B);
	X = A * B;
	printf("PROD = %d\n", X);
	return 0;
}
