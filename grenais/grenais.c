/**
 *	      @file: grenais.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int gre, nal, vicGre=0, vicNal=0, emp=0, resp;
	do{
		scanf("%d %d", &nal, &gre);
		if(gre>nal){
			vicGre++;
		} else if(nal > gre){
			vicNal++;
		} else {
			emp++;
		}
		printf("Novo grenal (1-sim 2-nao)\n");
		scanf("%d", &resp);
	}while(resp!=2);
	printf("%d grenais\nInter:%d\nGremio:%d\nEmpates:%d\n", vicGre+vicNal+emp,vicNal,vicGre,emp);
	if(vicGre>vicNal){
		printf("Gremio venceu mais\n");
	} else if(nal > gre){
		printf("Inter venceu mais\n");
	} else {
		printf("Nao houve vencedor\n");
	}

	return 0;
}
