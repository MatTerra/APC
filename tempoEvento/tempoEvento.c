/**
 *	      @file: tempoEvento.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int dia, diaFim, hora, horaFim, minuto, minutoFim, segundo, segundoFim;
	int dias, horas, minutos, segundos;
	scanf("Dia %d\n%d : %d : %d\nDia %d\n%d : %d : %d", &dia,&hora,&minuto,&segundo,&diaFim,&horaFim,&minutoFim,&segundoFim);
	dias = diaFim - dia;
	if(horaFim<hora){
		horas = (24-hora)+horaFim;
		dias--;
	} else {
		horas = horaFim-hora;
	}
	if(minutoFim<minuto){
		minutos = (60-minuto)+minutoFim;
		horas--;
	} else {
		minutos = minutoFim-minuto;
	}
	if(segundoFim<segundo){
		segundos = (60-segundo)+segundoFim;
		minutos--;
	} else {
		segundos = segundoFim-segundo;
	}
	printf("%d dia(s)\n%d hora(s)\n%d minuto(s)\n%d segundo(s)\n", dias, horas, minutos, segundos);
	return 0;
}
