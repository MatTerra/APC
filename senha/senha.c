/**
 *	      @file: senha.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int senha=2002, senhaDigitada;
	scanf("%d", &senhaDigitada);
	while(senhaDigitada!=senha){
		printf("Senha Invalida\n");
		scanf("%d", &senhaDigitada);
	}
	printf("Acesso Permitido\n");
	return 0;
}
