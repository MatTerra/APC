'''	      @file: lucas.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
vertebracao = input()
classe = input()
alimentacao = input()

if vertebracao =="vertebrado":
    if classe == "ave":
        if alimentacao == "aguia": #if alimentacao == 'carnivoro': *
            print("aguia")
        elif alimentacao == "onivoro": #else:
            print("pomba")
    elif classe == "mamifero": #else
        if alimentacao == "onivoro":
            print("homem")
        elif alimentacao == "herbivoro": #else:
            print("vaca")
elif vertebracao == "invertebrado": #else:
    if classe == "inseto":
        if alimentacao == "hematofago":
            print("pulga")
        elif alimentacao == "herbivoro": #else:
            print("lagarta")
    elif classe == "anelideo": #else:
        if alimentacao == "hematofago":
            print("sanguessuga")
        elif alimentacao == "onivoro": #else:
            print("minhoca")
