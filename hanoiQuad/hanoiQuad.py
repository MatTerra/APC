'''	      @file: hanoiQuad.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
def is_square(apositiveint):
	if apositiveint==1: return True
	x = apositiveint // 2
	seen = set([x])
	while x * x != apositiveint:
		x = (x + (apositiveint // x)) // 2
		if x in seen: return False
		seen.add(x)
	return True

n=int(input())
for i in range(n):
	v=int(input())
	if v>100: 
		print('-1')
	else:
		can=True;
		num=1;
		varetas=[0]*v;
		while(can==True):
			for i in range(len(varetas)):
				if is_square(varetas[i]+num) or varetas[i]==0:
					varetas[i]=num
					break
			else:
				can=False
			num+=1
		print(num-2)
