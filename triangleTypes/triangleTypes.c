/*	      @file: triangleTypes
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>
double l[3];

void bubbleSort(){
	int trocando = 1;
	while(trocando == 1){
		trocando =0;
		int x = 0;
		while( x<2){
			if(l[x]<l[x+1]){
				double a = l[x+1];
				l[x+1] = l[x];
				l[x] = a;
				trocando = 1;
			}
			x++;
		}
	}
}

int main(){
	scanf("%lf %lf %lf",&l[0],&l[1],&l[2]);
	bubbleSort();
	if(l[0]>=l[1]+l[2]){
		printf("NAO FORMA TRIANGULO\n");
	}else{
		if (l[0]*l[0] == (l[1]*l[1])+(l[2]*l[2])){
			printf("TRIANGULO RETANGULO\n");
		}else if (l[0]*l[0] > (l[1]*l[1])+(l[2]*l[2])){
			printf("TRIANGULO OBTUSANGULO\n");
		}else if (l[0]*l[0] < (l[1]*l[1])+(l[2]*l[2])){
			printf("TRIANGULO ACUTANGULO\n");
		} 
		if(l[0] == l[1] && l[1] == l[2]){
			printf("TRIANGULO EQUILATERO\n");
		} else if(l[0] == l[1] || l[1] == l[2]){
			printf("TRIANGULO ISOSCELES\n");
		}
	}
	return 0;
}
