/*	      @file: triangleTypes
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
double l[3];

void sort(){
	double swap, soma;
        soma = l[0]+l[1];
        swap = (l[0]+l[1]+abs(l[0]-l[1]))/2;
        l[1] = soma-swap;
        l[0] = swap;
        swap = (l[0]+l[2]+abs(l[0]-l[2]))/2;
        soma = l[2]+l[0];
        l[2] = soma-swap;
        l[0] = swap;
        swap = (l[1]+l[2]+abs(l[1]-l[2]))/2;
        soma = l[2]+l[1];
        l[2] = soma-swap;
        l[1] = swap;
}

int main(){
	scanf("%lf %lf %lf",&l[0],&l[1],&l[2]);
	sort();
	if(l[0]>l[1]+l[2]){
		printf("NAO FORMA TRIANGULO\n");
	}else{
		if (l[0]*l[0] == (l[1]*l[1])+(l[2]*l[2])){
			printf("TRIANGULO RETANGULO\n");
		}else if (l[0]*l[0] > (l[1]*l[1])+(l[2]*l[2])){
			printf("TRIANGULO OBTUSANGULO\n");
		}else if (l[0]*l[0] < (l[1]*l[1])+(l[2]*l[2])){
			printf("TRIANGULO ACUTANGULO\n");
		} 
		if(l[0] == l[1] && l[1] == l[2]){
			printf("TRIANGULO EQUILATERO\n");
		} else if(l[0] == l[1] || l[1] == l[2]){
			printf("TRIANGULO ISOSCELES\n");
		}
	}
	return 0;
}
