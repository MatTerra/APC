/**
 *	      @file: fibonnaciFacil.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

unsigned long long fib[1000];


unsigned long long fibonacci(int n){
	if(n==0){
		fib[n]=0;
		return fib[n];
	}else if(n<3){
		fib[n]=1;
		return fib[n];
	}
	fib[n] = (fib[n-1]!=-1?fib[n-1]:fibonacci(n-1))+(fib[n-2]!=-1?fib[n-2]:fibonacci(n-2));
	return fib[n];
}

int main(){
	int T, i, n=0;
	scanf("%d", &T);
	for(i=0; i<=700; i++)
        	fib[i] = -1;
	while(T--){
		scanf("%d",&n);
		printf("Fib(%d) = %llu\n", n, fibonacci(n));
	}
	return 0;
}
