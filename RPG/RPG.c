/**
 *	      @file: RPG.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *	Jogo de RPG textual elaborado para o trabalho 1 da Disciplina
 */

#include <stdio.h>
#include <stdlib.h>

int menu(), menuDificuldade(), primeiroPasso(int vidas), lerResposta(), lerRespostaSemZero();
void    limparTela(),
	mandarMensagem(int vidas, char mensagem[]),
	mandarMensagemComEspera(int vidas, char mensagem[]),
	mandarMensagemComOpcoes(int vidas, char mensagem[], char opcao1[], char opcao2[], char opcao3[]);

int main(){
	int vidas = 3;
	limparTela(vidas);
	mandarMensagemComEspera(vidas,"fsgboi");
	mandarMensagem(vidas, "gsdibg");
	vidas = menu();
	return 0;
}


/* função que reúne o código q muda a quantidade de vidas/dificuldade */
int menuDificuldade(){
	mandarMensagemComOpcoes(0, "Qual o seu nível de inteligência, calouro? (eu até podia perguntar seu nome e personalizar a experiência de jogo, mas calouro parece apropriado)", "Caloulo bulo", "Até sou inteligente, passei pra engenharia, né?", "Muuuito inteligente. Até sei o que é engenharia!");
	int dificuldade = lerResposta();
	return 4-dificuldade;
}





/* Primeiro passo da aventura */
int primeiroPasso(int vidas){
	mandarMensagemComEspera(vidas, "Você está no banheiro quando...\n\nBAAM\n\nA porta se abre de repente e você começa a ouvir uma voz sussurando palavras incompreensíveis de forma estressada.");
	mandarMensagemComOpcoes(vidas, "FLING\n\nVocê escuta um barulho, como uma arma sendo carregada. O que fazer?", "Sair e confrontar a voz", "Pegar sua arma e espiar a voz para tentar identificá-la","Esperar a voz sumir");
	switch(lerResposta()){
		case 1:
		case 2:
		case 3:
		break;
	}
	return 1; 
}


