/*	      @file: salaryIncrease
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float salary, raise;
	int percentual;
	scanf("%f", &salary);
	if (salary<=400.00){
		raise = 15*salary/100;
		percentual = 15;
	} else if (salary<=800.00){
		raise = 12*salary/100;
		percentual = 12;
	} else if (salary<=1200.00){
		raise = 10*salary/100;
		percentual = 10;
	} else if (salary<=2000.00){
		raise = 7*salary/100;
		percentual = 7;
	} else{
		raise = 4*salary/100;
		percentual = 4;
	}
	printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: %d \%\n", (raise+salary), raise, percentual);
	return 0;
}
