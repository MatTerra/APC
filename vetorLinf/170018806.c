/**      @file: vetor.c
 *     @author: Guilherme N. Ramos (gnramos@unb.br)
 * @disciplina: Algoritmos e Programação de Computadores
 *
 * Implemente as funções de modo que o código funcione. */

#include <stdio.h>

/* Pede um número inteiro positivo N ao usuário e o retorna se for válido
 * (0 < N <= 100), repetindo o pedido caso contrário. Assume que o usuário
 * insere apenas valores inteiros. */
int le_tamanho_do_vetor() {
	/* Exemplo:
	 *
	 * Digite N (0 < N <= 100): -1
	 * Número inválido.
	 * Digite N (0 < N <= 100): 101
	 * Número inválido.
	 * Digite N (0 < N <= 100): 5      <- retorna 5 */
	int N;
	printf("Digite N (0 < N <= 100): ");
	scanf("%d", &N);
	while(N>100 || N<=0){
		printf("Número inválido.\n");
		printf("Digite N (0 < N <= 100): ");
		scanf("%d", &N);
	}
	while(getchar()!='\n');
	return N;
}

/* Pede uma letra ao usuário e retorna o caractere equivalente se for válido, ou
 * seja, se for uma letra do alfabeto latino, repetindo o pedido caso contrário.
 * Assume que o usuário insere apenas caracteres ASCII. */
char uma_letra() {
	/* Exemplo:
	 *
	 * Digite uma letra: a             <- retorna 'a'
	 *
	 * Digite uma letra: 1
	 * Letra inválida.
	 * Digite uma letra: L             <- retorna 'L' */
	char c;
	printf("Digite uma letra: ");
	scanf("%c", &c);
	while(!((c>'a' && c<'z') || (c>'A' && c<'Z'))){
		printf("Letra inválida.");
		printf("Digite uma letra: ");
		scanf("%c", &c);
	}
	while(getchar()!='\n');
	return c;
}

/* Lê N elementos e os armazena no vetor dado, na mesma ordem em que são lidos.
 * Assume que o vetor comporta pelo menos N elementos e que o usuário só insere
 * dados válidos (caracteres ASCII). */
void le_elementos(char *vetor, int N) {
	/* Supondo o vetor[5]:
	 *
	 * le_elementos(vetor, 3);
	 *     -> Usuário insere "car"   -> vetor == {'c', 'a', 'r', ?, ?}
	 *
	 * le_elementos(vetor, 4);
	 *     -> Usuário insere "carr"  -> vetor == {'c', 'a', 'r', 'r', ?}
	 *
	 * le_elementos(vetor, 5);
	 *     -> Usuário insere "carro" -> vetor == {'c', 'a', 'r', 'r', 'o'} */
	int cont;
	printf("Digite %d caracteres: ", N);
	for(cont=0; cont<N; cont++){
		*(vetor+cont) = getchar();
	}
	while(getchar()!='\n');
}

/* Mostra o N primeiros elementos do vetor dado. Assume que N nunca é maior que
 * a quantidade total de elementos no vetor (e também que N >= 1). */
void mostra(char *vetor, int N) {
	/* Supondo o vetor {'c', 'a', 'r', 'r', 'o'}:
	 *
	 * mostra(vetor, 3); -> "car"
	 * mostra(vetor, 4); -> "carr"
	 * mostra(vetor, 5); -> "carro"
	 *
	 * Deve mostrar as aspas. */
	int cont;
	putchar('"');
	for(cont=0; cont<N; cont++){
		putchar(*(vetor+cont));
	}
	putchar('"');
	putchar('\n');
}

/* Ordena os N primeiros elementos do vetor dado em ordem crescente. Assume que
 * N nunca é maior que a quantidade total de elementos no vetor (e também que
 * N >= 1). */
void ordena(char *vetor, int N) {
	/* Supondo o vetor {'c', 'a', 'r', 'r', 'o'}:
	 *
	 * ordena(vetor, 3); -> vetor == {'a', 'c', 'r', 'r', 'o'}
	 * ordena(vetor, 4); -> vetor == {'a', 'c', 'r', 'r', 'o'}
	 * ordena(vetor, 5); -> vetor == {'a', 'c', 'o', 'r', 'r'}
	 *
	 * Existem diversos algoritmos para ordenação, alguns simples outros nem
	 * tanto. Uma ideia de fácil implementação é simplesmente comparar dois
	 * elementos consecutivos no vetor e, caso estejam fora de ordem, trocá-los
	 * de posição. Este processo precisa ser repetido algumas vezes até que o
	 * vetor esteja ordenado. */
	int trocando = 1;
	char swap;
	while(trocando){
		trocando=0;
		int i=0;
		while(i<N-1){
			if(*(vetor+i) > *(vetor+(i+1))){
				trocando = 1;
				swap=*(vetor+i);
				*(vetor+i)=*(vetor+(i+1));
				*(vetor+(i+1))=swap;
			}
			i++;
		}
	}

}

/* Busca o caractere dado no vetor e retorna o índice da primeira ocorrência
 * dele no vetor, se houver, -1 caso contrário. Assume que N nunca é maior que
 * a quantidade total de elementos no vetor (e também que N >= 1). */
int indice_de(char *vetor, int N, char c) {
	/* Supondo o vetor {'c', 'a', 'r', 'r', 'o'}:
	 *
	 * indice_de(vetor, 3, 'o') == -1
	 * indice_de(vetor, 4, 'o') == -1
	 * indice_de(vetor, 5, 'o') == 4
	 * indice_de(vetor, 5, 'c') == 0
	 * indice_de(vetor, 5, 'a') == 1
	 * indice_de(vetor, 5, 'r') == 2
	 *
	 * Existem diversas formas de buscar um elemento em um vetor, a mais simples
	 * é começar do início (como disse o Rei), e ir até o fim do vetor. Mas
	 * certas condições permitem que se faça uma busca mais eficiente, no estilo
	 * do Método da Bissecção... */
	int i;
	for(i=0;i<N;i++){
		if (c==*(vetor+i))
			return i;
	}
	return -1;
}

/* Pede uma letra ao usuário e a busca no vetor dado. Assume que N nunca é maior
 * que a quantidade total de elementos no vetor (e também que N >= 1). */
void busca_letra(char *vetor, int N) {
	char c = uma_letra();
	int i = indice_de(vetor, N, c);

	if(i >= 0)
		printf("\nO caractere '%c' está na %da posição do vetor.\n", c, i);
	else
		printf("\nO caractere '%c' não está no vetor.\n", c);
}

/* Apenas um exemplo de uso das funções acima. */
int main() {
	char vetor[100];
	int N = le_tamanho_do_vetor();

	le_elementos(vetor, N);
	printf("\nO vetor é: ");
	mostra(vetor, N);
	busca_letra(vetor, N);

	ordena(vetor, N);
	printf("\nO vetor ordenado é: ");
	mostra(vetor, N);
	busca_letra(vetor, N);

    return 0;
}

/* Exemplo de teste:
 *
 * Digite N (0 < N <= 100): 10
 *
 * Digite 10 caracteres: EisABanana
 *
 * O vetor é: "EisABanana"
 * Digite uma letra: n
 *
 * O caractere 'n' está na 6a posição do vetor.
 *
 * O vetor ordenado é: "ABEaaainns"
 * Digite uma letra: x
 *
 * O caractere 'x' não está no vetor.
 **/
