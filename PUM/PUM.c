/**
 *	      @file: PUM.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n, a=1;
	scanf("%d", &n);
	while(n--){
		while(a%4 != 0){
			printf("%d ",a);
			a++;
		}
		printf("PUM\n");
		a++;
	}
	return 0;
}
