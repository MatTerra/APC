/**
 *	      @file: simpleSort.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int sorted[3], input[3];

void bubbleSort(){
	int t = 1;
	int swap;
	while(t==1){
		t=0;
		int x=0;
		while(x<2){
			if(sorted[x] > sorted[x+1]){
				t = 1;
				swap=sorted[x];
				sorted[x]=sorted[x+1];
				sorted[x+1]=swap;
			}
			x++;
		}
	}
}

int main(){
	scanf("%d %d %d",&input[0], &input[1], &input[2]);
	int c = 0;
	while (c<3){
		sorted[c] = input[c];
		c++;
	}
	bubbleSort();
	printf("%d\n%d\n%d\n\n%d\n%d\n%d\n", sorted[0],sorted[1],sorted[2], input[0],input[1],input[2]);
	return 0;
}
