/**
 *	      @file: ultrapassandoZ.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int x, z, cont=0, sum=0;
	scanf("%d",&x);
	do{
		scanf("%d",&z);
	}while(z<=x);
	while(sum<=z){
		sum+=x;
		cont++;
		x++;
	}
	printf("%d\n",cont);
	return 0;
}
