/**
 *	      @file: desvioPM.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	double m,m1,m2,m3,m4,m5;
	scanf("%lf %lf %lf %lf %lf %lf", &m, &m1, &m2, &m3, &m4, &m5);
	m1 = m1-m;
	m1 = m1*m1;
        m2 = m2-m;
        m2 = m2*m2;
        m3 = m3-m;
        m3 = m3*m3;
        m4 = m4-m;
        m4 = m4*m4;
        m5 = m5-m;
        m5 = m5*m5;
	m=(m1+m2+m3+m4+m5)/20;
	printf("%.4lf", m);
	return 0;
}
