/**
 *	      @file: mediasPonderadas.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float a,b,c;
	int pesos[]={2,3,5}, somaPesos=10,n,cont;
	scanf("%d", &n);
	for(cont=n;n>0;n--){
		scanf("%f %f %f", &a, &b, &c);
		printf("%.1f\n",(a*pesos[0]+b*pesos[1]+c*pesos[2])/somaPesos);
	}
	return 0;
}
