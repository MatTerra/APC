/**
 *	      @file: reservaMel.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float V, D, R, H, A, pi=3.14;
	while (!feof(stdin)){
		scanf("%f\n%f", &V, &D);
		R = D/2;
		A = pi*R*R;
		H = V/A;
		printf("ALTURA = %.2f\nAREA = %.2f\n", H, A);
	}
	return 0;
}
