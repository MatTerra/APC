'''	      @file: sumImp.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
s=int(input())
e=int(input())
if s>e:
	swap = s
	s=e
	e=swap
if not s%2==0:
	s=s+2
else:
	s=s+1
soma=0
for i in range(s,e,2):
	soma=soma+i
print(soma)
