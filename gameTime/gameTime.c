/*	      @file: gameTime
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int start, end, duration;
	scanf("%d %d", &start, &end);
	if(end > start){
		duration = end-start;
	} else if( end < start ){
		duration = (24-start)+end;
	} else {
		duration = 24;
	}
	printf("O JOGO DUROU %d HORA(S)\n", duration);
	return 0;
}
