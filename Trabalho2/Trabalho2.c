/**
 *	      @file: Trabalho2.c
 *	    @author: Mateus Berardo de Souza Terra 1Mancalas/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *  Jogo de Mancala segundo regras descritas no trabalho 2
 */

#include <stdio.h>
#include <stdlib.h>

void display(int tabuleiro[4][7], char nomes[4][21], int player), initTabuleiro(int tabuleiro[4][7]), lerNomes(char nomes[4][21]), printResultados(int tabuleiro[4][7], char nomes[4][21]);
int *jogada(int tabuleiro[4][7], int kahala, int player), lerJogada(char nome[]), menu(), lerResposta(),esvaziou(int tabuleiro[7]), somaPontos(int linha[7], int linha2[7]);
char toUpper(char c);

int main(){
    const int NumPlayers=4, Mancalas = 7, MaxLen = 21;
    char nomes[NumPlayers][MaxLen];
    int tabuleiro[NumPlayers][Mancalas],
    game = 0, player=0, numJogada;
    int* ultimaKahala;
    while(game!=2){
        game=menu();
        if(game==1){
            lerNomes(nomes);
            initTabuleiro(tabuleiro);
            player=0;
            while(game){
                display(tabuleiro, nomes, player);
                numJogada=lerJogada(nomes[player]);
                while(tabuleiro[player][numJogada]==0){
                    printf("Escolha uma kahala com sementes!!\n");
                    numJogada=lerJogada(nomes[player]);
                }
                ultimaKahala=jogada(tabuleiro, numJogada, player);
                if(*(ultimaKahala+1) != -1 && tabuleiro[*(ultimaKahala)][*(ultimaKahala+1)]==1 && *(ultimaKahala+1)!=6){
                    printf("%s capturou %d sementes!!\n", nomes[player], tabuleiro[(*(ultimaKahala)+2)%4][5-*(ultimaKahala+1)]);
                    getchar();
                    tabuleiro[player][6]+=tabuleiro[(*(ultimaKahala)+2)%4][5-*(ultimaKahala+1)];
                    tabuleiro[(*(ultimaKahala)+2)%4][5-*(ultimaKahala+1)]=0; 
                }
                if(*(ultimaKahala+1) == 6)
                    player=(player+2)%4;
                if(*(ultimaKahala+1)!=6)
                    player=(player+1)%4;
                if(esvaziou(tabuleiro[0])||esvaziou(tabuleiro[1])||esvaziou(tabuleiro[2])||esvaziou(tabuleiro[3])){
                    game=0;
                }
            }
            printResultados(tabuleiro, nomes); 
            getchar();           
        }
    }
	return 0;
}

void initTabuleiro(int tabuleiro[4][7]){
    const int NumPlayers=4, SementesIniciais=4;
    int i, j;
    for(i=0;i<NumPlayers;i++){
        for(j=0; j<6; j++){
            tabuleiro[i][j]=SementesIniciais;   
        }
        tabuleiro[i][j]=0;
    }
}

void display(int tabuleiro[4][7], char nomes[4][21], int player){
    system("clear||cls");
    printf("\n\n");
    printf("                       %20s\n", nomes[0]);
    printf("                      K0   F   E   D   C   B   A\n");
    printf("                     +---+---+---+---+---+---+---+---+\n");
    printf("                     |%3d|%3d|%3d|%3d|%3d|%3d|%3d|%3d| K3\n", tabuleiro[0][6], tabuleiro[0][5], tabuleiro[0][4], tabuleiro[0][3], tabuleiro[0][2], tabuleiro[0][1], tabuleiro[0][0], tabuleiro[3][6]);
    printf("                     +---+---+---+---+---+---+---+---+\n");
    printf("                    A|%3d|                       |%3d| F\n", tabuleiro[1][0], tabuleiro[3][5]);
    printf("                     +---+                       +---+\n");
    printf("                    B|%3d|                       |%3d| E\n", tabuleiro[1][1], tabuleiro[3][4]);
    printf("                     +---+                       +---+\n");
    printf("                    C|%3d|                       |%3d| D\n", tabuleiro[1][2], tabuleiro[3][3]);
    printf("%20s +---+                       +---+     %s\n", nomes[1], nomes[3]);
    printf("                    D|%3d|                       |%3d| C\n",tabuleiro[1][3], tabuleiro[3][2]);
    printf("                     +---+                       +---+\n");
    printf("                    E|%3d|                       |%3d| B\n", tabuleiro[1][4], tabuleiro[3][1]);
    printf("                     +---+                       +---+\n");
    printf("                    F|%3d|                       |%3d| A\n", tabuleiro[1][5], tabuleiro[3][0]);
    printf("                     +---+---+---+---+---+---+---+---+\n");
    printf("                   K1|%3d|%3d|%3d|%3d|%3d|%3d|%3d|%3d| \n", tabuleiro[1][6], tabuleiro[2][0], tabuleiro[2][1], tabuleiro[2][2], tabuleiro[2][3], tabuleiro[2][4], tabuleiro[2][5], tabuleiro[2][6]);
    printf("                     +---+---+---+---+---+---+---+---+\n");
    printf("                           A   B   C   D   E   F  K2\n");
    printf("                       %20s\n", nomes[2]);
}

void lerNomes(char nomes[4][21]){
    int i;
    for(i=0; i<4; i++){
        system("clear||cls");
        printf("Jogador %d, escreva seu nome(20 caracteres):\n",i);
        scanf("%20[^\n]",nomes[i]);
        while(getchar() != '\n');
    }
}

void printResultados(int tabuleiro[4][7], char nomes[4][21]){
    system("clear||cls");
    int time1=somaPontos(tabuleiro[0], tabuleiro[2]),
        time2=somaPontos(tabuleiro[1], tabuleiro[3]);
    printf("%s e %s recolheram juntos %d sementes!\n", nomes[0], nomes[2], time1);
    printf("%s e %s recolheram juntos %d sementes!\n", nomes[1], nomes[3], time2);
    if(time1!=time2){
        printf("Parabéns, %s e %s!!! Vocês são os mestres do mancala!!\n", time1>time2?nomes[0]:nomes[1], time1>time2?nomes[2]:nomes[3]);
        printf("%s e %s, aprendam com eles!", time1<time2?nomes[0]:nomes[1], time1<time2?nomes[2]:nomes[3]);
    }else{
        printf("Vocês empataram! Parabéns para todos!!");
    }
}

int somaPontos(int linha[7], int linha2[7]){
    int soma=0,i;
    for(i=0; i<7;i++){
        soma+=linha[i];
        soma+=linha2[i];
    }
    return soma;
}

int menu(){
    system("clear||cls");
    printf(" .----------------.  .----------------.  .-----------------. .----------------.  .----------------.  .----------------.  .----------------.\n");
    printf("| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |\n");
    printf("| | ____    ____ | || |      __      | || | ____  _____  | || |     ______   | || |      __      | || |   _____      | || |      __      | |\n");
    printf("| ||_   \\  /   _|| || |     /  \\     | || ||_   \\|_   _| | || |   .' ___  |  | || |     /  \\     | || |  |_   _|     | || |     /  \\     | |\n");
    printf("| |  |   \\/   |  | || |    / /\\ \\    | || |  |   \\ | |   | || |  / .'   \\_|  | || |    / /\\ \\    | || |    | |       | || |    / /\\ \\    | |\n");
    printf("| |  | |\\  /| |  | || |   / ____ \\   | || |  | |\\ \\| |   | || |  | |         | || |   / ____ \\   | || |    | |   _   | || |   / ____ \\   | |\n");
    printf("| | _| |_\\/_| |_ | || | _/ /    \\ \\_ | || | _| |_\\   |_  | || |  \\ `.___.'\\  | || | _/ /    \\ \\_ | || |   _| |__/ |  | || | _/ /    \\ \\_ | |\n");
    printf("| ||_____||_____|| || ||____|  |____|| || ||_____|\\____| | || |   `._____.'  | || ||____|  |____|| || |  |________|  | || ||____|  |____|| |\n");
    printf("| |              | || |              | || |              | || |              | || |              | || |              | || |              | |\n");
    printf("| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |\n");
    printf(" '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------' \n\n\n");
    printf(" _                   _  ____  _____ ____  ____ \n");
    printf("/ \\                 / |/  _ \\/  __//  _ \\/  __\\\n");
    printf("| |    _____        | || / \\|| |  _| / \\||  \\/|\n");
    printf("| |    \\____\\    /\\_| || \\_/|| |_//| |-|||    /\n");
    printf("\\_/              \\____/\\____/\\____\\\\_/ \\|\\_/\\_\\\n");
    printf("                                               \n");
    printf(" ____            ____  ____  _  ____ \n");
    printf("/_   \\          / ___\\/  _ \\/ \\/  __\\\n");
    printf(" /   /  _____   |    \\| / \\|| ||  \\/|\n");
    printf("/   /_  \\____\\  \\___ || |-||| ||    /\n");
    printf("\\____/          \\____/\\_/ \\|\\_/\\_/\\_\\\n");
    printf("                                     \n");
    
    return lerResposta();
}

/* abstracao para ler a resposta do usuario */
int lerResposta(){
	int resposta;
	do{
		scanf("%d", &resposta);
		if (resposta>2 || resposta<1){
			printf("\nTá errado!! Quer dizer.. tá certo.. Mas tá errado, tem que ser um número de 1 a 2!\n");
		}
		while(getchar()!='\n');
	}while(resposta > 2 || resposta < 1);
	return resposta;
}

int* jogada(int tabuleiro[4][7], int kahala, int player){
    static int kahalaFinal[2];
    int i=player, j=kahala, sementes=tabuleiro[player][kahala];
    tabuleiro[player][kahala]=0;
    while(sementes--){
        j++;
        if(j>5 && i%2!=player%2){
            i++;
            j=0;
        }
        else if(j>6){
            i++;
            j=0;
        }
        i=i%4;
        tabuleiro[i][j]++;
    }
    kahalaFinal[0] = i;
    kahalaFinal[1] = (i%2==player%2?j:-1);
    return kahalaFinal;
    
}

int eLetraAceita(char c){
    return (c>='A' && c<='F')||(c>='a'&&c<='f');
}

int lerJogada(char nome[]){
    char kahala;
    int invalida=1;
    printf("\n\n");
    printf("    %s, é sua vez de jogar! Escolha uma Kahala(A-F):\n",nome);
    do{
        scanf("%c", &kahala);
        if(getchar()=='\n'){
            if(eLetraAceita(kahala)){
                invalida=0;
            } else {
                printf("Insira apenas uma letra entre A e F!!!\n");
            }
        }
        else{
           while(getchar()!='\n');
           printf("Insira apenas uma letra entre A e F!!!\n"); 
        }
    }while(invalida);
    return toUpper(kahala)-'A';
}

int esvaziou(int tabuleiro[7]){
    int j,r=1;
        for(j=0; j<6 && r==1;j++)
            if(tabuleiro[j]!=0)
                r=0;
    return r;
}

/* supõe uma entrada que é uma letra */
char toUpper(char c){
    if(c>='a')
        return c-('a'-'A');
    return c;
}
