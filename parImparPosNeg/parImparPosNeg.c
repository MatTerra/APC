/**
 *	      @file: parImparPosNeg.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int pos=0;
	int n[5], cont=0, par=0, imp=0, neg=0;
	while(cont<5){
		scanf("%d", &n[cont]);
		if(n[cont]%2==0)
			par++;
		else
			imp++;
		if(n[cont] == 0){
		}else if (n[cont] > 0){
			pos++;
		} else {
			neg++;
		}
		cont++;
	}
	printf("%d valor(es) par(es)\n", par);
	printf("%d valor(es) impar(es)\n", imp);
	printf("%d valor(es) positivo(s)\n", pos);
	printf("%d valor(es) negativo(s)\n", neg);
	return 0;
}
