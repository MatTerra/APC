/**
 *	      @file: somaImparesConsecutivos.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n, start, quant, aux, cont, soma=0;
	scanf("%d", &n);
	while(n--){
		soma=0;
		scanf("%d %d", &start, &quant);
		if(start%2==0) start++;
		for(cont=0;cont<quant;cont++){
			soma+=(start+(cont*2));
		}
		printf("%d\n", soma);
	}
	return 0;
}
