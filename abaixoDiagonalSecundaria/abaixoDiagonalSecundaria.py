'''	      @file: abaixoDiagonalSecundaria.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores

	
'''
#include <stdio.h>

if __name__ == "__main__":
	op = input('')

	mat = [[] for i in range(12)]
	
	for line in mat: 
		for i in range(12):
			line.append(float(input('')))

	soma = 0
	n = 0
	for index, line in enumerate(mat):
		for column in line[12-index:]:
			soma += column
			n += 1	

	print("{:.1f}".format(soma if op=='S' else soma/n))