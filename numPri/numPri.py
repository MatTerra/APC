'''	      @file: numPri.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
n=int(input())
pri=(2,3,5,7,11,13,17,19,23,29)
for i in range(n):
	num=int(input())
	if not num in pri:
		for l in pri:
			if num%l==0:
				print("%d nao eh primo"%num)
				break
		else:
			print("%d eh primo"%num)
	else:
		print("%d eh primo"%num)
