'''	      @file: quadradoPar.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
N = int(input())
for i in range(2,N+1,2):
	print(str(i)+"^2 = "+str(i*i))
