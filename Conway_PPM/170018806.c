/**      @file: Conway_PPM.c
 *     @author: Guilherme N. Ramos (gnramos@unb.br)
 * @disciplina: Algoritmos e Programação de Computadores
 *
 * Implementação do "Jogo da Vida" de Conway, complete o código das funções
 * atualiza e gera_ppm. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Poucas linhas de modo que "caiba na tela"... Quando todo o código estiver
 * pronto, tente comentar o código para exibição na tela e analisar arquivos com
 * dimensões maiores. */
#define LINHAS 480
#define COLUNAS 640

#define VIVO '*'
#define MORTO ' '

typedef char status;
typedef status campo[LINHAS][COLUNAS];

/* Inicializa com o padrão "blinker". */
void blinker(campo m) {
	int i = LINHAS/2, j = COLUNAS/2;
	m[i-1][j] = m[i][j] = m[i+1][j] = VIVO;
}

/* Inicializa com o padrão "beacon". */
void beacon(campo m) {
	int i = LINHAS/2, j = COLUNAS/2;
	m[i-2][j-2] = m[i-2][j-1] = VIVO;
	m[i-1][j-2] = m[i-1][j-1] = VIVO;
	m[i+1][j+1] = m[i+1][j] = VIVO;
	m[i][j+1] = m[i][j] = VIVO;
}

/* Inicializa com o padrão "Figure 8". */
void figure8(campo m) {
	int i = LINHAS/2, j = COLUNAS/2;
	m[i-2][j-2] = m[i-2][j-1] = m[i-2][j] = VIVO;
	m[i-1][j-2] = m[i-1][j-1] = m[i-1][j] = VIVO;
	m[i][j-2] = m[i][j-1] = m[i][j] = VIVO;
	m[i+1][j+1] = m[i+1][j+2] = m[i+1][j+3] = VIVO;
	m[i+2][j+1] = m[i+2][j+2] = m[i+2][j+3] = VIVO;
	m[i+3][j+1] = m[i+3][j+2] = m[i+3][j+3] = VIVO;
}

/* Inicializa com o padrão "glider". */
void glider(campo m) {
    m[1][3] = VIVO;
	m[2][1] = m[2][3] = VIVO;
	m[3][2] = m[3][3] = VIVO;
}

void random(campo m){
    int i,j;
    srand((unsigned)time(NULL));
    for(i=0; i<LINHAS; i++){
        for(j=0; j<COLUNAS; j++){
            m[i][j]=rand()%20==0?VIVO:MORTO;
        }
    }
}

/* Inicializa todas as células do campo dado conforme a opção de entrada dada. */
void inicializa(campo m, char opcao) {
	memset(m, MORTO, sizeof(campo)); /* todas a células mortas */

	switch(opcao) {
	    case '1':
	        random(m);
	        break;
		case '2':
			beacon(m);
			break;
		case '3':
			figure8(m);
			break;
		case '4':
			glider(m);
			break;
		default:
			blinker(m);
		}
}

/* Escreve o campo na saída padrão. */
void exibe(campo m) {
	int i, j;

	for(i = 0; i < LINHAS; ++i) {
		for(j = 0; j < COLUNAS; ++j)
			printf("%c", m[i][j]);
		printf("\n");
	}
}

/* Atualiza o campo conforme as regras do jogo:
 *
 * 1) Qualquer célula viva com menos de dois vizinhos vivos morre de solidão.
 * 2) Qualquer célula viva com mais de três vizinhos vivos morre de
 * superpopulação.
 * 3) Qualquer célula morta com exatamente três vizinhos vivos se torna uma
 * célula viva.
 * 4) Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo
 * estado para a próxima geração. */
void atualiza(campo m) {
	/* Atenção, uma célula que esteja na borda do campo não possui vizinho(s)
	 * além da borda. Por exemplo, uma célula de canto tem apenas os 3 vizinhos
	 * "para dentro" do campo.*/
	int i, j, numVivos=0;
	campo aux;
	for(i = 0; i < LINHAS; ++i) {
		for(j = 0; j < COLUNAS; ++j){
		    numVivos=0;
		    if(i>0){
                if(m[i-1][j]==VIVO) numVivos++;
                if(m[i-1][j+1]==VIVO) numVivos++;
                if(j>0){
                    if(m[i-1][j-1]==VIVO) numVivos++;
                }
		    }
		    if(j>0){
		        if(m[i][j-1]==VIVO) numVivos++;
                if(m[i+1][j-1]==VIVO) numVivos++;
		    }
		    if(i<LINHAS-1){
		        if(m[i+1][j]==VIVO) numVivos++;
		        if(j<COLUNAS-1)
		            if(m[i+1][j+1]==VIVO) numVivos++;
		    }
		    if(j<COLUNAS-1){
		        if(m[i][j+1]==VIVO) numVivos++;
		    }
		    
		    if(numVivos > 3 && m[i][j]==VIVO){
		        aux[i][j]=MORTO;
		    } else if(numVivos < 2 && m[i][j]==VIVO){
		        aux[i][j]=MORTO;
		    } else if(numVivos == 3 && m[i][j]==MORTO) {
		        aux[i][j]=VIVO;
		    } else {
		        aux[i][j]=m[i][j];
		    }
		}
	}
	for(i = 0; i < LINHAS; ++i) {
		for(j = 0; j < COLUNAS; ++j)
		    m[i][j]=aux[i][j];
	}
	
}

/* Gera um arquivo PPM do campo dado. O nome do arquivo deve ser conway-##.ppm
 * onde ## representa dois dígitos com o valor do contador passado como
 * argumento. Por exemplo, cont = 0 gera o arquivo conway-00.ppm, cont = 37 gera
 * o arquivo conway-37.ppm. Assuma que 0 <= cont < 100. */
void gera_ppm(campo m, int cont) {
    char nome[14];
    int ida=0;
    sprintf(nome, "conway-%02d.ppm", cont);
    FILE *ppm = fopen(nome, "w+");
    int l, c;
    if(ppm){
        fprintf(ppm, "P3\n%d %d\n255\n", COLUNAS, LINHAS);
        for(l=0; l < LINHAS; l++){
            for(c=0; c < COLUNAS; c++){
                if(cont%5==0) ida=!ida;
                if(ida){
                    if(m[l][c]== VIVO) fprintf(ppm, "%d 0 %d\n", (255/4)*(cont%5), 255-((255/4)*(cont%5)));
                    else fprintf(ppm, "30 30 30\n");
                }else{
                     if(m[l][c]== VIVO) fprintf(ppm, "%d 0 %d\n", 255-((255/4)*(cont%5)),(255/4)*(cont%5));
                     else fprintf(ppm, "30 30 30\n");
                }
                
            }
        }
        fclose(ppm);
    } else {
        printf("Não foi possível gerar o arquivo %d!", cont);
    }
}

/* Laço principal de execução. A configuração inicial depende do segundo
 * argumento dado. */
int main(int argc, char **argv) {
	int i, ciclos = 1;
	campo jogo_da_vida;

	inicializa(jogo_da_vida, argc < 2 ? '0' : argv[1][0]);
    
	do {
		printf("\nDigite a quantidade de ciclos:");
		scanf("%d", &ciclos);
	} while(0 < ciclos && ciclos >= 100);
    while(getchar()!='\n');
	for(i = 1; i <= ciclos; ++i) {
		system("clear");
		printf("Ciclo %d:\n", i);
		exibe(jogo_da_vida);
		printf("\n\nPressione [Enter] para continuar.");
		getchar();

		gera_ppm(jogo_da_vida, i);

		atualiza(jogo_da_vida);
	}

    return 0;
}
