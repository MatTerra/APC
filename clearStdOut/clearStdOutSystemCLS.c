/**
 *	      @file: clearStdOut.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
int main(){
	while(1){
	int cont=0;
	while(cont<20){
		printf("...\n");
		cont++;
	}
	printf("pressione enter para limpar\n");
	getchar();
	system("clear");
	}
	return 0;
}
