/*	      @file: media3
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	const int weights[] = {2,3,4,1};
	int wSum=0;
	float notas[4], ne, media, mediaFinal;
	int cont=0;
	scanf("%f %f %f %f", &notas[0], &notas[1], &notas[2], &notas[3]);
	while(cont < 4){
		media += notas[cont]*weights[cont];
		wSum += weights[cont];
		cont++;
	}
	media = media/wSum;
	printf("Media: %.1f\n", media);
	if(media>7.0){
		printf("Aluno aprovado. \n");
	} else if (media < 5.0){
		printf("Aluno reprovado. \n");
	} else {
                printf("Aluno em exame. \n");
		scanf("\n%f", &ne);
		mediaFinal = (media+ne)/2;
		if (mediaFinal>=5.0){
        	        printf("Nota do exame: %.1f\n", ne);
			printf("Aluno aprovado. \n");
		} else {
        	        printf("Nota do exame: %.1f\n", ne);
			printf("Aluno reprovado. \n");
		}
		printf("Media Final: %.1f\n", mediaFinal);
	}
	return 0;
}
