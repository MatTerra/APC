/**
 *	      @file: waitForKeyPress.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	printf("Pressione qqr tecla para sair...");
	getchar();
	return 0;
}
