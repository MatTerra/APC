/**
 *	      @file: trocaVetor.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

void troca(int *n1, int *n2){
	int aux = *n1;
	*n1=*n2;
	*n2=aux;
}

int main(){
	int N[20],i;
	for(i=0;i<20;i++){
		scanf("%d\n", &N[i]);
	}
	for(i=0;i<10;i++){
		troca(&N[i], &N[19-i]);
	}
	for(i=0;i<20;i++){
		printf("N[%d] = %d\n", i, N[i]);
	}
	return 0;
}
