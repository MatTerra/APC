/**
 *	      @file: pares.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *	Recebe 5 numeros e mostra quantos sao pares
 *
 */

#include <stdio.h>

int main(){
	int n[5], cont=0, par=0;
	while(cont<5){
		scanf("%d", &n[cont]);
		if(n[cont]%2==0)
			par++;
		cont++;
	}
	printf("%d valores pares\n", par);
	return 0;
}
