/**
 *	      @file: fatorialDinamico.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

double fact[500];


double factorial(int n){
	if(n==0 || n == 1){
		fact[n]=1;
		return fact[n];
	}
	fact[n] = n*(fact[n-1]!=0?fact[n-1]:factorial(n-1));
	return fact[n];
}

int main(){
	int n, i, cont=1;
	char end=' ';
	scanf("%d", &n);
	for(i=0; i<=cont; i++)
        	fact[i] = 0; 
	while(cont<=n){
		if(cont==n) end = '\n';
		printf("%.0lf%c", factorial(cont),end);
		cont++;
	}
	return 0;
}
