/*            @file: simpleCalculate.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *	Calcula o preco a pagar baseado na entrada do preco e da 
 *	quantidade de dois itens
 */

#include <stdio.h>

int code, quant, x;
double preco, total;

int main(){
	total = 0;
	x=0;
	while(x < 2){
		scanf("%d %d %lf", &code, &quant, &preco);
		total = total+(quant*preco);
		x++;
	}
	printf("VALOR A PAGAR: R$ %.2lf\n", total);
	return 0;
}

