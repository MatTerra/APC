/**
 *	      @file: menorPos.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n, pos, i;
	scanf("%d", &n);
	int list[n];
	scanf("%d", &list[0]);
	pos=0;
	for(i=1; i<n; i++){
		scanf("%d",&list[i]);
		if(list[i]<list[pos])
			pos=i;
	}
	printf("Menor valor: %d\nPosicao: %d\n", list[pos], pos);
	return 0;
}
