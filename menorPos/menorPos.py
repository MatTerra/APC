'''	      @file: menorPos.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
pos=0
min=0
n=int(input())
a=input().split(" ")
min=int(a[0])
for i in range(1, len(a)):
	b=int(a[i])
	if b < min :
		pos=i
print('Menor valor: %d'%int(a[pos]))
print('Posicao: %d'%pos)
