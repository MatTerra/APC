/*	      @file: average3
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float n1, n2, n3, n4, ne, media, mediaFinal;
	scanf("%f %f %f %f", &n1, &n2, &n3, &n4);
	media = ((2.0*n1)+(3.0*n2)+(4.0*n3)+n4)/10.0;
	printf("Media: %.1f\n", media);
	if (media >= 7.0){
		printf("Aluno aprovado.\n");
	} else {
		if(media < 5.0){
			printf("Aluno reprovado.\n");
		} else {
			if(media>=5.0 && media<=6.9){
				printf("Aluno em exame.\n");
				scanf("%f", &ne);
				printf("Nota do exame: %.1f\n",ne);
				mediaFinal = (media+ne)/2;
				if (mediaFinal>=5.0){
					printf("Aluno aprovado.\n");
				} else {
					printf("Aluno reprovado\n");
				}
			}
			printf("Media final: %.1f\n", mediaFinal);
		}
	}
	return 0;
}
