/*		@file: diferenca.c
 *	      @author: Mateus Berardo de Souza Terra 17/0018806
 *	  @disciplina: Algoritmos e Programacao de Computadores
 *
 * 	Recebe quatro inteiros A, B, C e D e mostra a diferenca dos produtos A*B e C*D na saida padrao
 *
 */

#include <stdio.h>

int A, B, C, D, diferenca;

int main(){
	scanf("%d\n%d\n%d\n%d", &A, &B, &C, &D);
	diferenca = (A*B)-(C*D);
	printf("DIFERENCA = %d\n", diferenca);
	return 0;
}
