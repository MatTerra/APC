#include <stdio.h>
void display(char matriz[3][4]){
    int i;
    for(i=0; i<3; i++){
            printf("%3s\n", matriz[i]);
    }
}

void lerNomes(char nomes[3][4]){
    int i,j;
    for(i=0; i<3; i++){
        printf("Jogador %d, escreva seu nome(20 caracteres):\n",i);
        scanf("%3[^\n]",nomes[i]);
        for(j=0; j<4;j++)
            printf("%c", nomes[i][j]);
        while(getchar() != '\n');
    }
}

int main(){
    char matriz[3][4];  
    lerNomes(matriz); 
    display(matriz);
    return 0;
}
