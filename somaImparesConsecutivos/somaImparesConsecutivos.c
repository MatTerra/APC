/**
 *	      @file: somaImparesConsecutivos.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n, start, end, aux, cont, soma=0;
	scanf("%d", &n);
	for(;n>0;n--){
		soma=0;
		scanf("%d %d", &start, &end);
		if(start>end){
			aux = end;
			end = start;
			start = aux;
		}
		for(cont=start+1;cont<end;cont++){
			if(cont%2!=0)
				soma+=cont;
		}
		printf("%d\n", soma);
	}
	return 0;
}
