/**
 *	      @file: validacaoDeNota.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

float lerNota(){
	float i;
	scanf("%f",&i);
	while(i>10.0 || i<0){
		printf("nota invalida\n");
		scanf("%f",&i);
	}
	return i;
}
void calcularMedia(){
	float n1, n2;
	n1=lerNota();
	n2=lerNota();
	printf("media = %.2f\n", (n1+n2)/2);
}
int main(){
	int r;
	calcularMedia();
	do{
		printf("novo calculo (1-sim 2-nao)\n");
		scanf("%d", &r);
		if(r==1) calcularMedia();
	} while(r!=2);
	return 0;
}
