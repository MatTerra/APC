/**
 *	      @file: seqCres.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n, termo;
	scanf("%d", &n);
	do{
		for(termo = 1; termo <= n; termo++){
			printf("%d%c", termo, termo==n?'\n':' ');
		}
		scanf("%d", &n);
	}while(n!=0);
	return 0;
}
