/**
 *	      @file: selecaoVetor.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float A[100];
	int i=0;
	for(;i<=99;i++){
		scanf("%f\n",&A[i]);
	}
	for(i=0;i<100;i++){
		if(A[i]<=10.0) printf("A[%d] = %.1f\n", i, A[i]);

	}
	return 0;
}
