/*	      @file: interval.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float n;
	scanf("%f",&n);
	if(n>25.00001){
		if (n>50.00001){
			if(n>75.00001){
				if(n>100.00001){
					printf("Fora de intervalo\n");
				}else{
					printf("Intervalo (75,100]\n");
				}
			}else{
				printf("Intervalo (50,75]\n");
			}
		}else{
			printf("Intervalo (25,50]\n");
		}
	}else{
		if(n>=0.0)
			printf("Intervalo [0,25]\n");
		else
			printf("Fora de intervalo\n");
	}
	return 0;
}
