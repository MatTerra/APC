'''	      @file: preenVetor4.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores

'''
par=0
impar=0
listPar=[0,0,0,0,0]
listImpar=[0,0,0,0,0]
for i in range(15):
	n=int(input())
	if(n%2==0):
		if(par < 5):
			listPar[par]=n
		else:
			for i in range(par):
				print('par[%d]'%i, end=" = ")
				print(listPar[i])
			par=0
			listPar[par]=n
		par+=1
	else:
		if(impar < 5):
			listImpar[impar]=n
		else:
			for i in range(impar):
				print('impar[%d]'%i, end=" = ")
				print(listImpar[i])
			impar=0
			listImpar[impar]=n
		impar+=1
for i in range(impar):
	print('impar[%d]'%i, end=" = ")
	print(listImpar[i])
for i in range(par):
	print('par[%d]'%i, end=" = ")
	print(listPar[i])

