/**
 *	      @file: acimaDiagonalPrincipal.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	double matriz[12][12], soma;
	int i,j,n=0;
	char op;
	scanf("%c",&op);
	for(i=0; i<12; i++){
		for(j=0; j<12; j++){
			scanf("%lf",&matriz[i][j]);
		}
	}
	for(j=0; j<5; j++){
		for(i=j+1; i<11-j; i++){
			soma+=matriz[i][j];
			n++;
		}
	}
	printf("%.1lf\n", op=='S'?soma:soma/n);

	return 0;
}
