/*	      @file: consumption
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int X;
	float Y;
	scanf("%d\n%f", &X, &Y);
	printf("%.3f km/l\n", X/Y);
	return 0;
}
