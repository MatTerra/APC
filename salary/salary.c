/*	      @file: salary.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *	Recebe o numero do trabalhador, o numero de horas trabalhadas
 *	e o salario por hora. Calcula o salario do mes e imprime o 
 *	numero do trabalhador e seu salario
 */

#include <stdio.h>

int numTrabalhador, numHoras;
double salarioHora, salarioMes;

int main(){
	scanf("%d\n%d\n%lf", &numTrabalhador, &numHoras, &salarioHora);
	salarioMes = numHoras*salarioHora;
	printf("NUMBER = %d\nSALARY = U$ %.2f\n", numTrabalhador, salarioMes);
	return 0;
}
