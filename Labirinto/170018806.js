/* 17/0018806 Mateus Berardo de Souza Terra
 *
 * Resolução do jogo do Blockly, Labirinto
 * 
 * Conjunto das instruções necessárias para solucionar os 
 * desafios do jogo Labirinto.
 * https://blockly-games.appspot.com/maze?lang=pt-br
 */


/* Problema 1 */
moveForward();
moveForward();


/* Problema 2 */
moveForward();
turnLeft();
moveForward();
turnRight();
moveForward();


/* Problema 3 */
while (notDone()) {
  moveForward();
}


/* Problema 4 */
while (notDone()) {
  moveForward();
  turnLeft();
  moveForward();
  turnRight();
}


/* Problema 5 */
moveForward();
moveForward();
turnLeft();
while (notDone()) {
  moveForward();
}


/* Problema 6 */
while (notDone()) {
  moveForward();
  if (isPathLeft()) {
    turnLeft();
  }
}


/* Problema 7 */
while (notDone()) {
  moveForward();
  if (isPathRight()) {
    turnRight();
  }
}


/* Problema 8 */
while (notDone()) {
  moveForward();
  if (isPathRight()) {
    turnRight();
  }
  if (isPathLeft()) {
    turnLeft();
  }
}


/* Problema 9 */
while (notDone()) {
  if (isPathForward()) {
    moveForward();
  } else {
    turnLeft();
  }
}


/* Problema 10 */
while (notDone()) {
  moveForward();
  if (isPathLeft()) {
    if (isPathRight()) {
      if (isPathForward()) {
        turnRight();
      } else {
        turnLeft();
      }
    } else {
      turnLeft();
    }
  } else {
    if (isPathRight()) {
      turnRight();
    }
  }
}
