/**
 *	      @file: numerosPositivos.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float values[6], soma;
	int cont=0, pos=0;
	float media;
	while(cont<6){
		scanf("%f", &values[cont]);
		if(values[cont] > 0){
			pos++;
			soma+=values[cont];
		}
		cont++;
	}
	media = soma/pos;
	printf("%d valores positivos\n%.1f\n", pos, media);
	return 0;
}
