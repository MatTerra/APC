/**
 *	      @file: timeConversion.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int seconds, minutes, hours;
	scanf("%d", &seconds);
	minutes = seconds/60;
	seconds = seconds - (minutes*60);
	hours = minutes/60;
	minutes = minutes - (hours*60);
	printf("%d:%d:%d\n", hours, minutes, seconds); 
	return 0;
}
