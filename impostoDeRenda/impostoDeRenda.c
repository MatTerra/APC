/**
 *	      @file: impostoDeRenda.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float salary, faixa1=0,faixa2=0,faixa3=0;
	scanf("%f", &salary);
	if(salary<=2000.00){
		printf("Isento\n");
	} else {
		if (salary>4500.00){
			faixa3=(salary-4500.00)*0.28;
			salary=4500.00;
		}
		if (salary>3000.00){
			faixa2=(salary-3000.00)*0.18;
			salary = 3000.00;
		}
		if (salary>2000.00){
			faixa1=(salary-2000)*0.08;

		}
		printf("R$ %.2f\n", faixa1+faixa2+faixa3);
	}
	return 0;
}
