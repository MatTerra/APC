/**
 *	      @file: multiples.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
	int x, y, swap, soma;
	scanf("%d %d", &x,&y);
	soma = x+y;
	swap = (x+y+abs(x-y))/2;
	x=swap;
	y=soma-x;
	if(x%y!=0){
		printf("Nao sao Multiplos\n");
	} else {
		printf("Sao Multiplos\n");
	}
	return 0;
}
