/**      @file: palindromo.c
 *     @author: Mateus Berardo de Souza Terra 17/0018806
 * @disciplina: Algoritmos e Programação de Computadores
 *
 * Lê um número inteiro do usuário e indica se este é um
 * palíndromo ou não. Exemplos:
 *
 *  Entrada             | Saída
 *  --------------------+----------------------------
 *                    2 | É um número palíndromo.
 *                   10 | Não é um número palíndromo.
 *                   11 | É um número palíndromo.
 *                  100 | Não é um número palíndromo.
 *                 1001 | É um número palíndromo.
 *           1234567890 | Não é um número palíndromo.
 *  1234567890987654321 | É um número palíndromo.         */

#include <stdio.h>
#include <string.h>

int equals(char um, char outro),ePalindromo(char *num, int size);

int main() {
    char num[100];
    int size;
    
    scanf("%[0-9]",num);
    size = strlen(num);
    
    if(ePalindromo(num, size))
        printf("É um número palíndromo.\n");
    else
        printf("Não é um número palíndromo.\n");
    return 0;
}

int equals(char um, char outro){
    if (um==outro)
        return 1;
    return 0;
}

int ePalindromo(char *num, int size){
    if(size==1 || size==0)
        return 1;
    if(equals(num[0], num[size-1]))
        return (ePalindromo(num+1, size-2));
    return 0;
}
