/*	      @file: sphere
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

const double pi = 3.14159;
double R;

int main(){
	scanf("%lf", &R);
	printf("VOLUME = %.3lf\n",(4.0/3)*pi*R*R*R);
	return 0;
}
