/**
 *	      @file: numerosPositivos.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	float values[6];
	int cont=0, pos=0;
	while(cont<6){
		scanf("%f", &values[cont]);
		if(values[cont] > 0){
			pos++;
		}
		cont++;
	}
	printf("%d valores positivos\n", pos);
	return 0;
}
