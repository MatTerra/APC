/**
 *	      @file: cresPop.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int casos, pa, pb, i;
	double ca, cb;
	scanf("%d", &casos);
	while(casos--){
		scanf("%d %d %lf %lf", &pa, &pb, &ca, &cb);
		for(i=1; i<=100; i++){
			pa*=((100.0+ca)/100.0);
			pb*=((100.0+cb)/100.0);
			if(pa>pb){
				printf("%d anos.\n", i);
				break;
			}
		}
		if(i>100)
			printf("Mais de 1 seculo.\n");
	}
	return 0;
}
