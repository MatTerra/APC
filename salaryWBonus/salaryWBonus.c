/*	      @file: salary.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *	Recebe o numero do trabalhador, o numero de horas trabalhadas
 *	e o salario por hora. Calcula o salario do mes e imprime o 
 *	numero do trabalhador e seu salario
 */

#include <stdio.h>

double salario, vendas;
char nome[100];

int main(){
	scanf("%s\n%lf\n%lf", nome, &salario, &vendas);
	printf("TOTAL = R$ %.2lf\n", salario+(vendas*15/100));
	return 0;
}
