/**
 *	      @file: snack.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	const float preco[] = {4.00, 4.5, 5.0, 2.0, 1.5};
	int X, Y;
	scanf("%d %d", &X, &Y);
	printf("Total: R$ %.2f\n", Y*preco[X-1]);
	return 0;
}
