'''	      @file: mult13.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
n1=int(input())
n2=int(input())
inc = 0
if n1<n2:
	inc += 1
else:
	inc -= 1
sum=0
for i in range(n1,n2+inc, inc):
	if i % 13 != 0:
		sum+=i
print("%d"%sum)
