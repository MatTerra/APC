/**
 *	      @file: funcoes.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int rafael(int x, int y){
	return (9*(x*x))+(y*y);
}

int beto(int x, int y){
	return (2*(x*x))+(25*y*y);
}

int carlos(int x, int y){
	return (-100*x)+(y*y*y);
}

int main(){
	int cont, x, y, r, b, c;
	scanf("%d", &cont);
	while(cont--){
		scanf("%d %d", &x, &y);
		r=rafael(x,y);
		b=beto(x,y);
		c=carlos(x,y);
		if(r>c&&r>b){
			printf("Rafael ganhou\n");
		} else if(c>b) {
			printf("Carlos ganhou\n");
		} else {
			printf("Beto ganhou\n");
		}
	}	
	return 0;
}
