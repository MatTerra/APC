/*	      @file: fuelSpent
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	const float rend = 12.0;
	int t, v;
	scanf("%d\n%d",&t, &v);
	printf("%.3f",(v*t)/rend); 
	return 0;
}
