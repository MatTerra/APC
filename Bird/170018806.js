/* 17/0018806 Mateus Berardo de Souza Terra
 *
 * Resolução do jogo do Blockly, Bird
 * 
 * Conjunto das instruções necessárias para solucionar os 
 * desafios do jogo Bird.
 * https://blockly-games.appspot.com/maze?lang=pt-br
 */


/* Problema 1 */
heading(45);


/* Problema 2 */
if (noWorm()) {
  heading(0);
} else {
  heading(90);
}


/* Problema 3 */
if (noWorm()) {
  heading(300);
} else {
  heading(60);
}


/* Problema 4 */
if (getX() < 80) {
  heading(0);
} else {
  heading(270);
}


/* Problema 5 */
if (getY() < 20) {
  heading(180);
} else {
  heading(270);
}


/* Problema 6 */
if (noWorm()) {
  heading(345);
} else if (getY() < 80) {
  heading(90);
} else {
  heading(180);
}


/* Problema 7 */
if (getY() > 55) {
  heading(210);
} else if (noWorm()) {
  heading(315);
} else {
  heading(180);
}


/* Problema 8 */
if (getX() < 50 && true) {
  heading(45);
} else if (noWorm()) {
  heading(315);
} else if (getY() < 50) {
  heading(135);
} else {
  heading(45);
}


/* Problema 9 */
if (noWorm() && getX() > 30) {
  heading(180);
} else if (noWorm()) {
  heading(255);
} else if (getY() < 60 && getX() < 30) {
  heading(90);
} else if (getX() < 50) {
  heading(0);
} else {
  heading(300);
}


/* Problema 10 */
if (noWorm() && getY() < 75 && getX() < 70) {
  heading(90);
} else if (getX() < 80 && noWorm()) {
  heading(0);
} else if (noWorm()) {
  heading(270);
} else if (getY() < 75 && getX() > 50) {
  heading(90);
} else if (getX() > 20) {
  heading(180);
} else {
  heading(270);
}
