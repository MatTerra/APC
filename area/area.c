/*	      @file: area
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	const double pi = 3.14159;
	double A, B, C;

	scanf("%lf %lf %lf", &A, &B, &C);
	printf("TRIANGULO: %.3lf\nCIRCULO: %.3lf\nTRAPEZIO: %.3lf\nQUADRADO: %.3lf\nRETANGULO: %.3lf\n",A*C/2, C*C*pi,(A+B)*C/2, B*B, A*B);
	return 0;
}
