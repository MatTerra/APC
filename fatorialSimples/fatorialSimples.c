/**
 *	      @file: fatorialSimples.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

float fatorial(int n){
	if(n==1)
		return 1;
	return n*fatorial(n-1);
}
int main(){
	int n;
	scanf("%d", &n);
	printf("%.0f\n", fatorial(n));
	return 0;
}
