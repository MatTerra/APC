/**
 *	      @file: triangulo.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>
int l[4];

void bubbleSort(){
	int trocando = 1;
	while(trocando == 1){
		trocando = 0;
		int x = 0;
		while(x<3){
			if(l[x]<l[x+1]){
				int a = l[x+1];
				l[x+1] = l[x];
				l[x] = a;
				trocando = 1;
			}
			x++;
		}
	}
}
int main(){
	int cont=0;
	while(cont<4){
		scanf("%d", &l[cont]);
		cont++;
	}
	bubbleSort();
	if((l[0] < l[1]+l[2]) || (l[0]<l[2]+l[3]) || l[1] < l[2]+l[3]){
		printf("S\n");
	} else {
		printf("N\n");
	}
	return 0;
}
