/**      @file: bin2txt.c
 *     @author: Guilherme N. Ramos (gnramos@unb.br)
 * @disciplina: Algoritmos e Programação de Computadores
 *
 * Mensagens secretas são mais interessantes que as outras, principalmente em
 * tempos de tanta insegurança... Atualmente, a solução para comunicação segura
 * é escrever uma mensagem, cifrá-la e enviá-la. Assim , qualquer pessoa mal
 * intencionada que interceptar a comunicação não vai entender o conteúdo.
 * Claro, isso só funciona se o receptor da mensagem souber decifrá-la...
 *
 * Faça um programa que receba como argumento da função main o nome de um
 * arquivo binário e leia deste arquivo uma quantidade indeterminada de
 * registros do tipo mensagem_secreta. Para cada registro, crie um arquivo texto
 * contendo apenas a mensagem secreta decifrada.
 *
 * A cifra é simples: deslocar cada caractere do string (até o caractere de
 * terminação - não incluso) a quantidade "segredo" de caracteres. Por exemplo,
 * um segredo 2 transforma o caractere 'a' em 'c'. Suponha que o segredo é
 * sempre válido (i.e. nunca gera um caractere inválido), ou seja, não se
 * preocupe com os valores. Segue junto a este trabalho o arquivo "teste.bin"
 * para testes. No exemplo, as mensagens decifradas são facilmente reconhecidas
 * e, para facilitar o desenvolvimento, saiba que o primeiro registro tem
 * segredo 0.
 *
 * Cada arquivo texto deve ter o nome conforme o seguinte padrão:
 * "nome_do_binario-#.txt", em que "nome_do_binario" é o nome do arquivo binário
 * de origem, e # é a posição do registro em questão neste arquivo binário. Como
 * exemplo, veja o arquivo "teste.bin-0.txt". Caso não seja possível criar um
 * arquivo texto, exiba uma mensagem indicando qual arquivo deixou de ser criado
 * e continue o processamento dos demais registros.
 *
 * Suponha que todos os caracteres da mensagem são ASCII.
 *
 ***********
 * ATENÇÃO *
 ***********
 * O registro é definido com a opção __attribute__ ((packed)). Você pode ignorar
 * isto e continuar a programar como sempre fez ou aproveitar a chance de
 * aprender algo novo e ler:
 *     https://en.wikipedia.org/wiki/Sizeof#Structure_padding                 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct {
	int segredo;
	char mensagem[101];
} __attribute__ ((packed)) mensagem_secreta;


char decifra(char c, int X) {
	return c-X;
}

char cifra(char c, int X){
    return decifra(c,0-X);
}

int main(int argc, char **argv) {
	/* Validação de entradas:
	   - testar se os argumentos são suficientes
	   - testar se os argumentos são válidos */
	int i, m=0;
	char decifrada[101];
	char nome_arquivo[200];
	mensagem_secreta msg;
    if(argc<2){
        printf("É necessário fornecer o nome de um arquivo!");
        return EXIT_FAILURE;
    }
	/* Leitura de quantidade indeterminada de registros do arquivo dado.
	 * (veja o que retorna a função fread) */
	if(argc == 2 && strcmp(argv[1],"help")){
        FILE *fp = fopen(argv[1],"rb");
        if(!fp){
            printf("Não foi possível abrir o arquivo fornecido, tente novamente.");
            return EXIT_FAILURE;
        }
        while(fread(&msg, sizeof msg, 1, fp)>0){
            for(i=0; msg.mensagem[i] != '\0'; i++){
                decifrada[i]=decifra(msg.mensagem[i], msg.segredo);
            }
            decifrada[i]=msg.mensagem[i];
            sprintf(nome_arquivo,"%s-%d.txt", argv[1],m);
            FILE *ftxt = fopen(nome_arquivo, "w+");
            if(!ftxt)
                printf("Não foi possível gravar a mensagem %d!", m);
            else{
                fprintf(ftxt, "%s", decifrada);
                fclose(ftxt);           
            }
            m++;
        }
        fclose(fp);
	    /* Escrita de cada mensagem em arquivo texto apropriado.
    	 * (veja a função sprintf) */
    }
    if(!strcmp(argv[1],"help")){
        printf("Uso:\n\tbin2txt <nome_do_arquivo>\n\nOptions:\n\n\t-c \n\tcria um arquivo binário\n\t\tUso: bin2txt -c <nome_do_bin> <quantidade_de_arquivos>\n\n\thelp \n\tdescreve o uso do programa\n\t\tUso: bin2txt help\n");
    }
    if(argc > 3 && !strcmp(argv[1],"-c")){
        int j;
        FILE *f;
        srand((unsigned)time(NULL));
        for(j=0;j<atoi(argv[3]);j++){
            sprintf(nome_arquivo, "%s-%d.txt", argv[2], j);
            msg.segredo=rand()%(255-'z');
            f = fopen(nome_arquivo,"r");
            fread(msg.mensagem, sizeof(char), 101, f);
            
        } 
    }
    return EXIT_SUCCESS;
}


