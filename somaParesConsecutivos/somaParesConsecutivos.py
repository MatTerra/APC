'''	      @file: somaParesConsecutivos.py
	    @author: Mateus Berardo de Souza Terra 17/0018806
	@disciplina: Algoritmos e Programacao de Computadores


'''
x=int(input())
while x!=0:
	if(x%2!=0):
		x+=1
	soma=0
	for i in range(x, x+10, 2):
		soma+=i
	print(soma)
	x=int(input())
