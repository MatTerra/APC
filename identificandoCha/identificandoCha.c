/**
 *	      @file: identificandoCha.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int T, cont=0, respCorreta=0, competidores[5];
	scanf("%d",&T);
	while(cont<5){
		scanf("%d", &competidores[cont]);
		if(competidores[cont] == T)
			respCorreta++;
		cont++;
	}
	printf("%d\n", respCorreta);
	return 0;
}
