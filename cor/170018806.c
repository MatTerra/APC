#include <stdio.h>

void colorir(char cor, char folha[1001][1001], int N), mostrarFolha(char folha[1001][1001], int N);

int main(){
    int N, i, MAXSIZE=1001;
    char folha[MAXSIZE][MAXSIZE], c='1';
    scanf("%d", &N);
    for(i=0; i<N;i++)
            scanf("%s", folha[i]);
    colorir(c, folha, N);
    mostrarFolha(folha, N);
    return 0;
}

void colorir(char cor, char folha[1001][1001], int N){
    int i,j, coloridos=0;
    for(i=0; i<N; i++)
        for(j=0; j<N; j++){
            if(folha[i][j] == cor-1 || (cor=='9' && folha[i][j]==cor)){
                if(i>0)
                    if(folha[i-1][j] == '*'){
                        folha[i-1][j]=cor;
                        coloridos++;
                    }
                if(folha[i+1][j]=='*'){
                    folha[i+1][j]=cor;
                    coloridos++;
                }
                if(j>0)
                    if(folha[i][j-1]=='*'){
                        folha[i][j-1]=cor;
                        coloridos++;
                    }
                if(folha[i][j+1]=='*'){
                    folha[i][j+1]=cor;
                    coloridos++;
                }
            }
        }
    if(coloridos!=0)
        colorir(cor<'9'?cor+1:cor, folha, N);
}

void mostrarFolha(char folha[1001][1001], int N){
    int i;
    for(i=0; i<N;i++)
        printf("%s\n", folha[i]);
}
