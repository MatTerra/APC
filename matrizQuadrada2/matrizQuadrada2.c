/**
 *	      @file: matrizQuadrada.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int ord,i,j,c;
	do{
		scanf("%d", &ord);
		int matriz[ord][ord];
		for(i=0; i < ord; i++){
			for(j=0; j < ord; j++){
				matriz[i][j]=1;
			}
		}
		for(i=c; i < ord-c; i++){
			for(j=c; j < ord-c; j++){
				matriz[i][j]+=1;			
			}
		}
		for(i=0; i < ord; i++){
			for(j=0; j < ord; j++){
				printf("%s%d%s", matriz[i][j]>9?" ":"  ",matriz[i][j],j==ord-1?(i==ord-1?"\n\n":"\n"):" ");
			}
		}
	}while(ord>0);
	return 0;
}
