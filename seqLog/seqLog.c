/**
 *	      @file: seqLog.c
 *	    @author: Mateus Berardo de Souza Terra 17/0018806
 *	@disciplina: Algoritmos e Programacao de Computadores
 *
 *
 */

#include <stdio.h>

int main(){
	int n,i=1;
	scanf("%d",&n);
	for(;i<=n;i++){
		int cont=0;
		while(cont<2){
			printf("%d %d %d\n",i,(i*i)+cont, (i*i*i)+cont);
			cont++;
		}
	}
	return 0;
}
